﻿/// <reference path="../references.ts" />

class Constants {

    public static gridSize: number = 30;
    public static prozessorPos: [number, number] = [19.5 * Constants.gridSize, 14.5 * Constants.gridSize];
    public static startingBits: number = 350;
    public static unitDamage: number = 1;
    public static unitValue: number = 12;

    //convert coordinates to a grid square
    public static XYToGrid(posX: number, posY: number): [number, number] {
        let lokalX: number = (posX) / this.gridSize;
        let lokalY: number = (posY - 60) / this.gridSize;
        if (lokalX < 0 || lokalY < 0) {
            return null;
        }
        lokalX = Math.floor(lokalX);
        lokalY = Math.floor(lokalY);
        return [lokalX, lokalY];
    }

    //convert a grid square to coordinates (middle of square)
    public static GridToXY(gridX: number, gridY: number): [number, number] {
        let lokalX: number = (gridX + 0.5) * this.gridSize;
        let lokalY: number = (gridY + 2.5) * this.gridSize;
        return [lokalX, lokalY];
    }

    //converts track from grid squares to coordinates (pixels)
    public static convertTrack(track: [number, number][]): void {
        track.forEach((e: [number, number]) => {
            e[0] *= Constants.gridSize;
            e[1] *= Constants.gridSize;
        });
    }
}