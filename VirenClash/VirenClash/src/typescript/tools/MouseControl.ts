﻿/// <reference path="../references.ts" />

class MouseControl {

    private image: HTMLImageElement;
    private towerType: TowerType;
    private game: Game;
    private canvas: HTMLCanvasElement;
    private dragging: boolean;
    private drawCoords: [number, number];

    constructor(game: Game, canvas: HTMLCanvasElement) {
        this.game = game;
        this.canvas = canvas;
        this.canvas.onmousedown = this.onDown.bind(this);
        this.canvas.onmouseleave = this.onLeave.bind(this);
        this.canvas.onmousemove = this.onMove.bind(this);
        this.canvas.onmouseup = this.onUp.bind(this);
    }

    private onDown(e: MouseEvent): void {
        this.drawCoords = [e.pageX - this.canvas.offsetLeft, e.pageY - this.canvas.offsetTop];

        //clicked pause/play?
        if ((this.drawCoords[0] > 15 * Constants.gridSize) && (this.drawCoords[0] <= 16 * Constants.gridSize) && (this.drawCoords[1] >= 0 * Constants.gridSize) && (this.drawCoords[1] <= 2 * Constants.gridSize)) {
            this.game.pausePlay();
        } else if (this.updatePickupType()) {
            //clicked on a towertype
            this.dragging = true;
        } else {
            //clicked on something else
            this.drawCoords = undefined;
            this.dragging = false;
        }
    }

    //tries to place tower if one was dragged
    private onUp(e: MouseEvent): void {
        if (this.dragging) {
            this.dragging = false;
            this.game.placeTower(this.towerType, this.drawCoords[0], this.drawCoords[1]);
            this.drawCoords = null;
            this.image = null;
        }
    }

    //update mouse position
    private onMove(e: MouseEvent): void {
        this.drawCoords = [e.pageX - this.canvas.offsetLeft, e.pageY - this.canvas.offsetTop];
    }


    private onLeave(e: MouseEvent): void {
        this.dragging = false;
        this.drawCoords = null;
    }

    private updatePickupType(): boolean {
        let towerType: TowerType = null;
        let image: HTMLImageElement = null;

        //check for x-range of the pickup area
            //check for the corresponding y-range for the towers
            //if one was clicked set type and picture
        if ((this.drawCoords[0] > 21 * Constants.gridSize) && (this.drawCoords[0] <= 24 * Constants.gridSize)) {
            if ((this.drawCoords[1] > 2 * Constants.gridSize) && (this.drawCoords[1] <= 4 * Constants.gridSize)) {
                towerType = TowerType.SniperTower;
                image = Pictures.sniperTowerDND;
            } else if ((this.drawCoords[1] > 4 * Constants.gridSize) && (this.drawCoords[1] <= 6 * Constants.gridSize)) {
                towerType = TowerType.MGTower;
                image = Pictures.mGTowerDND;
            } else if ((this.drawCoords[1] > 6 * Constants.gridSize) && (this.drawCoords[1] <= 8 * Constants.gridSize)) {
                towerType = TowerType.HomingTower;
                image = Pictures.homingTowerDND;
            } else if ((this.drawCoords[1] > 8 * Constants.gridSize) && (this.drawCoords[1] <= 10 * Constants.gridSize)) {
                towerType = TowerType.LightningTower;
                image = Pictures.lightningTowerDND;
            } else if ((this.drawCoords[1] > 10 * Constants.gridSize) && (this.drawCoords[1] <= 12 * Constants.gridSize)) {
                towerType = TowerType.BombTower;
                image = Pictures.bombTowerDND;
            } else if ((this.drawCoords[1] > 12 * Constants.gridSize) && (this.drawCoords[1] <= 14 * Constants.gridSize)) {
                towerType = TowerType.FrozenTower;
                image = Pictures.frozenTowerDND;
            } else if ((this.drawCoords[1] > 14 * Constants.gridSize) && (this.drawCoords[1] <= 16 * Constants.gridSize)) {
                towerType = TowerType.LinkTower;
                image = Pictures.connection;
            }
        }
        if (towerType != null && image != null) {
            this.towerType = towerType;
            this.image = image;
            return true;
        } else {
            this.towerType = null;
            this.image = null;
            return false;
        }
    }

    public getDrawImage(): HTMLImageElement {
        if (this.dragging) {
            return this.image;
        } else {
            return null;
        }
    }

    public getDrawCords(): [number, number] {
        return this.drawCoords;
    }
}