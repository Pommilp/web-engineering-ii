﻿/// <reference path="../references.ts" />

abstract class Tower {

    protected image: HTMLImageElement;
    private reloadTime: number;
    private coolDown: number;
    protected searchRadius: number;
    protected posX: number;
    protected posY: number;
    protected game: Game;

    constructor(game: Game, posX: number, posY: number, reloadTime: number, searchRadius: number, image: HTMLImageElement) {
        this.posX = posX;
        this.posY = posY;
        this.reloadTime = reloadTime;
        this.coolDown = 0;
        this.searchRadius = searchRadius;
        this.game = game;
        this.image = image;
    }

    //called every tick, used to update cooldown, call shoot
    //and reset cooldown if shoot was successful
    public tick(): void {
        if (this.coolDown > 0) {
            this.coolDown--;
        } else {
            let success: boolean = this.shoot();
            if (success) {
                this.resetCooldown();
            }
        }
    }

    public abstract shoot(): boolean;

    public resetCooldown(): void {
        this.coolDown = this.reloadTime;
    }

    public getImage(): HTMLImageElement {
        return this.image;
    }

    public getPosX(): number {
        return this.posX;
    }

    public getPosY(): number {
        return this.posY;
    }
}