﻿/// <reference path="../references.ts" />

class LinkTower extends Tower {

    constructor(game: Game, posX: number, posY: number) {
        super(game, posX, posY, 0, 0, null);
        this.game.addTower(this);
        //called in constructor because it is only done once at construction
        this.shoot();
    }

    public shoot(): boolean {
        new Connection(this.game, this.posX, this.posY, Balance.LinkHealth);
        return true;
    }

    //Override to prevent multiple Connection spawns
    public tick(): void { }
}