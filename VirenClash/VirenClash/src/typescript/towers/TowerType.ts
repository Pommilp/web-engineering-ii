﻿/// <reference path="../references.ts" />

enum TowerType {
    SniperTower,
    MGTower,
    HomingTower,
    LightningTower,
    BombTower,
    FrozenTower,
    LinkTower
}