﻿/// <reference path="../references.ts" />

class MGTower extends Tower {

    private bulletSpeed: number;
    private bulletDamage: number;

    constructor(game: Game, posX: number, posY: number) {
        super(game, posX, posY, Balance.MGRT, Balance.MGSR, Pictures.mGTower);
        this.bulletSpeed = Balance.MGBS;
        this.bulletDamage = Balance.MGBD;
        game.addTower(this);
    }

    //shoots if unit in range
    public shoot(): boolean {
        let target: Unit = this.game.getFirstUnitInRadius(this.searchRadius, this.posX, this.posY);
        if (target != null) {
            new StraightBall(this.game, target.getPosX(), target.getPosY(), this.bulletDamage, this.posX, this.posY, this.bulletSpeed);
            return true;
        } else {
            return false;
        }
    }
}