﻿/// <reference path="../references.ts" />

class HomingTower extends Tower {

    private bulletSpeed: number;
    private bulletDamage: number;

    constructor(game: Game, posX: number, posY: number) {
        super(game, posX, posY, Balance.HomingRT, Balance.HomingSR, Pictures.homingTower);
        this.bulletSpeed = Balance.HomingBS;
        this.bulletDamage = Balance.HomingBD;
        game.addTower(this);
    }

    //shoots if unit in range
    public shoot(): boolean {
        let target: Unit = this.game.getFirstUnitInRadius(this.searchRadius, this.posX, this.posY);
        if (target != null) {
            new DirectedBall(this.game, target, this.bulletDamage, this.posX, this.posY, this.bulletSpeed);
            return true;
        } else {
            return false;
        }
    }
}