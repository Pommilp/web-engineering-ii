﻿/// <reference path="../references.ts" />

class FrozenTower extends Tower {
    private frozenPeriod: number;
    private radius: number;
    private cooldown: number;

    constructor(game: Game, posX: number, posY: number) {
        super(game, posX, posY, Balance.FrozenRT, 0, Pictures.frozenTower);
        this.frozenPeriod = Balance.FrozenPeriod;
        this.radius = Balance.FrozenRadius;
        game.addTower(this);
    }

    //doesn't matter wether units are in range or not
    public shoot(): boolean {
        new FrozenSurface(this.game, this.posX, this.posY, this.frozenPeriod, this.radius);
        return true;
    }
}
