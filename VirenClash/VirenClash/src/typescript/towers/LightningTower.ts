﻿/// <reference path="../references.ts" />

class LightningTower extends Tower {

    private lightningColor: string;
    private lightningDamage: number;
    private lightningMaxBounces: number;
    private lightningBounceTime: number;

    constructor(game: Game, posX: number, posY: number) {
        super(game, posX, posY, Balance.LightningRT, Balance.LightningSR, Pictures.lightningTower);
        this.lightningColor = '#EE82EE';
        this.lightningDamage = Balance.LightningDamage;
        this.lightningMaxBounces = Balance.LightningMaxBounces;
        this.lightningBounceTime = Balance.LightningBounceTime;
        game.addTower(this);
    }

    //shoots if unit in range
    public shoot(): boolean {
        let target: Unit = this.game.getFirstUnitInRadius(this.searchRadius, this.posX, this.posY);
        if (target != null) {
            new Lightning(this.game, target, this.lightningDamage, this.posX, this.posY, this.lightningMaxBounces, this.searchRadius, this.lightningBounceTime, this.lightningColor);
            return true;
        } else {
            return false;
        }
    }
}