﻿/// <reference path="../references.ts" />

class BombTower extends Tower {

    private bulletSpeed: number;
    private explosionDamage: number;
    private burnDamage: number;
    private burnTime: number;

    constructor(game: Game, posX: number, posY: number) {
        super(game, posX, posY, Balance.BombRT, Balance.BombSR, Pictures.bombTower);
        this.bulletSpeed = Balance.BombBS;
        this.explosionDamage = Balance.BombBD;
        this.burnDamage = Balance.BombBurnDamage;
        this.burnTime = Balance.BombBurnTime;
        game.addTower(this);
    }

    //shoots if unit in range
    public shoot(): boolean {
        let target: Unit = this.game.getFirstUnitInRadius(this.searchRadius, this.posX, this.posY);
        if (target != null) {
            new ExplosiveShot(this.game, target.getPosX(), target.getPosY(), this.posX, this.posY, Balance.BombBurnRadius, this.bulletSpeed, this.explosionDamage, this.burnDamage, this.burnTime);
            return true;
        } else {
            return false;
        }
    }
}