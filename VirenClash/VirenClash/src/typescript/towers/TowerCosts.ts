﻿/// <reference path="../references.ts" />

class TowerCosts {

    public static getCostByType(type: TowerType): number {
        switch (type) {
            case TowerType.SniperTower:
                return 300;
            case TowerType.MGTower:
                return 200;
            case TowerType.HomingTower:
                return 450;
            case TowerType.LightningTower:
                return 750;
            case TowerType.BombTower:
                return 950;
            case TowerType.FrozenTower:
                return 600;
            case TowerType.LinkTower:
                return 400;
        }
    }
}