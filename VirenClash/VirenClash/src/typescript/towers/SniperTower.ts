﻿/// <reference path="../references.ts" />

class SniperTower extends Tower {

    private bulletSpeed: number;
    private bulletDamage: number;
    private damageFactor: number;

    constructor(game: Game, posX: number, posY: number) {
        super(game, posX, posY, Balance.SniperRT, Balance.SniperSR, Pictures.sniperTower);
        this.bulletSpeed = Balance.SniperBS;
        this.bulletDamage = Balance.SniperBD;
        this.damageFactor = Balance.SniperDamageFactor;
        game.addTower(this);
    }

    //shoots if unit in range
    public shoot(): boolean {
        let target: Unit = this.game.getFirstUnitInRadius(this.searchRadius, this.posX, this.posY);
        if (target != null) {
            new SniperShot(this.game, target.getPosX(), target.getPosY(), this.bulletDamage, this.damageFactor, this.posX, this.posY, this.bulletSpeed);
            return true;
        } else {
            return false;
        }
    }
}