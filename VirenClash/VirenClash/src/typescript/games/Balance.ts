﻿/// <reference path="../references.ts" />

class Balance {
    //this class is used to maintain all the values to balance the game
    //so it is easier to keep an overview af the values and to find them
    /*RT = ReloadTime
    * SR = SearchRadius
    * BS = BulletSpeed
    * BD = BulletDamage
    * FrozenSF = Frozen Surface
    */
    public static HomingRT = 60;
    public static HomingSR = 100;
    public static HomingBS = 5;
    public static HomingBD = 7;

    public static SniperRT = 120;
    public static SniperSR = 150;
    public static SniperBS = 200;
    public static SniperBD = 50;
    public static SniperDamageFactor = 0.7;

    public static MGRT = 20;
    public static MGSR = 130;
    public static MGBS = 40;
    public static MGBD = 5;

    public static FrozenPeriod = 300;
    public static FrozenRT = Balance.FrozenPeriod + 300;
    public static FrozenRadius = 45;

    public static BombRT = 90;
    public static BombSR = 100;
    public static BombBS = 8;
    public static BombBD = 9;
    public static BombBurnDamage = 0.3;
    public static BombBurnTime = 50;
    public static BombBurnRadius = 30;

    public static LightningRT = 160;
    public static LightningSR = 70;
    public static LightningDamage = 20;
    public static LightningMaxBounces = 10;
    public static LightningBounceTime = 5;

    public static FrozenSFFreezeFactor = 0;
    public static FrozenSFFreezeTime = 180;
    public static FrozenSFCoolFactor = 0.5;
    public static FrozenSFCoolTime = 180;

    public static LinkHealth = 10;
}