﻿/// <reference path="../references.ts" />

class Stats {
    private waveReached: number;
    private towersPlaced: number;
    private unitsKilled: number;
    private bitsEarned: number;

    constructor() {
        this.waveReached = 0;
        this.towersPlaced = 0;
        this.unitsKilled = 0;
        this.bitsEarned = 0;
    }

    public addWaveReached(amount: number): void {
        this.waveReached += amount;
    }

    public addTowerPlaced(amount: number): void {
        this.towersPlaced += amount;
    }

    public addUnitKilled(amount: number): void {
        this.unitsKilled += amount;
    }

    public addBitsEarned(amount: number): void {
        this.bitsEarned += amount;
    }

    public getStats(): [number, number, number, number]{
        return [this.waveReached, this.towersPlaced, this.unitsKilled, this.bitsEarned];
    }
}