﻿/// <reference path="../references.ts" />

class Processor {

    private maxHealth: number;
    private health: number;
    private image: HTMLImageElement;
    private imageDestroyed: HTMLImageElement;

    constructor(maxhealth: number) {
        this.maxHealth = maxhealth;
        this.health = maxhealth;
        this.image = Pictures.processor;
        this.imageDestroyed = Pictures.processorDestroyed
    }

    public doDamage(amount: number): void {
        this.health -= amount;
    }

    public getHealthPercentage(): number {
        return (this.health / this.maxHealth);
    }

    public getImages(): [HTMLImageElement, HTMLImageElement, number] {
        return [this.image, this.imageDestroyed, (this.health / this.maxHealth)];
    }
}