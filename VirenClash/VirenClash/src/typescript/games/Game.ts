﻿/// <reference path="../references.ts" />

class Game {

    //this var is used to safe a link to the requestAnimationFrame Loop, to be able to destroy it at stopLoop()
    private loopStorage: number = undefined;
    //start koordinates of the map
    private readonly startX: number;
    private readonly startY: number;
    private canvas: HTMLCanvasElement;
    private context: CanvasRenderingContext2D;
    private backgroundImage: HTMLImageElement;
    //hold all Units
    private units: Unit[];
    //hold all Towers
    private towers: Tower[];
    //hold all Shoots
    private shots: Shot[];

    private grid: any[21][14];

    private debugSlow: number;

    //storage of the track
    private readonly track: [number, number][];

    public stats: Stats;

    //contains and inits drag & drop functionalities
    private mouseControl: MouseControl;

    private processor: Processor;
    private bits: number = Constants.startingBits;

    private wave: Wave;

    private drawer: Drawer;

    private running: boolean;

    constructor(backgroundImage: HTMLImageElement, startX: number, startY: number, track: [number, number][], grid: [21][14], processorHealth: number) {
        this.backgroundImage = backgroundImage;
        this.startX = startX * Constants.gridSize;
        this.startY = startY * Constants.gridSize;
        this.canvas = <HTMLCanvasElement>document.getElementById("game-area");
        this.context = this.canvas.getContext("2d");
        this.units = new Array<Unit>();
        this.towers = new Array<Tower>();
        this.track = track;
        Constants.convertTrack(this.track);
        this.shots = new Array<Shot>();
        this.grid = grid;
        this.mouseControl = new MouseControl(this, this.canvas);
        Pictures.init();
        this.processor = new Processor(processorHealth);
        this.wave = new Wave(this);
        this.stats = new Stats();
        this.drawer = new Drawer(this);
        this.running = true;
        window.onresize = this.updatePositionNewGameButton.bind(this);
    }

    public startLoop(): void {
        if (this.loopStorage == undefined) {
            this.loopStorage = window.requestAnimationFrame(this.gameLoop.bind(this));
        }
    }

    public stopLoop(): void {
        window.cancelAnimationFrame(this.loopStorage);
        this.loopStorage = undefined;
    }

    //here is what happens in the game
    private gameLoop(): void {
        //this is used to slow the gameloop for debug purposes
        /*this.debugSlow++;
        if (this.debugSlow < 1) {
            this.loopStorage = window.requestAnimationFrame(this.gameLoop.bind(this));
            return;
        }
        this.debugSlow = 0;*/

        //actual gameloop code

        this.wave.tick();
        this.tickTowers();
        this.tickUnits();
        this.tickShots();
        this.checkHits();
        if (this.processor.getHealthPercentage() <= 0) {
            this.end();
            //end animation loop
            return;
        }
        this.drawer.draw();
        this.loopStorage = window.requestAnimationFrame(this.gameLoop.bind(this));
    }

    //calls tick on all shots and removes the ones out of the canvas
    private tickShots(): void {
        this.shots.forEach((shot) => {
            shot.tick();

            let x: number = shot.getPosX();
            let y: number = shot.getPosY();
            if (x < 0 || y < 0 || x > this.canvas.width || y > this.canvas.width) {
                this.removeShot(shot);
            }
        });
    }

    //calls tick on all units and removes the ones out of the canvas
    private tickUnits(): void {
        this.units.forEach((unit) => {

            unit.tick();
            

            let x: number = unit.getPosX();
            let y: number = unit.getPosY();
            if (x < 0 || y < 0 || x > this.canvas.width || y > this.canvas.height) {
                this.removeUnit(unit);
            }
        });
    }

    private tickTowers(): void {
        this.towers.forEach((tower) => {
            tower.tick();
        });
    }

    //checks for each shot if there is a unit to hit
    private checkHits(): void {
        this.shots.forEach((shot) => {
            let foundUnit: Unit = this.getUnitAt(shot.getPosX(), shot.getPosY());
            if (foundUnit != null) {
                shot.hit(foundUnit);
            }
        });
    }

    //walks through the track sectionwise until the given progress is reached
    public getPosFromProgress(prog: number): [number, number] {
        let newPos: [number, number] = this.getStart();
        let move: number = prog;
        for (var i = 0; i < this.track.length; i++) {
            let steps = Math.abs(this.track[i][0] + this.track[i][1]);
            if (move >= steps) {
                newPos[0] += this.track[i][0];
                newPos[1] += this.track[i][1];
                move -= steps;
            } else {
                if (this.track[i][0] != 0) {
                    if (this.track[i][0] > 0) {
                        newPos[0] += move;
                    } else {
                        newPos[0] -= move;
                    }
                } else {
                    if (this.track[i][1] > 0) {
                        newPos[1] += move;
                    } else {
                        newPos[1] -= move;
                    }
                }
                move = 0;
                i = this.track.length;
            }
        }
        if (move > 0) {
            //unit reached the end of the track
            this.processor.doDamage(Constants.unitDamage);
            return null;
        }
        return newPos;
    }

    //returns the first unit in the units array at the given position
    private getUnitAt(posX: number, posY: number): Unit {
        let foundUnit: Unit = null;
        this.units.forEach((unit) => {
            if (posX <= (unit.getPosX() + 15) && posX >= (unit.getPosX() - 15) &&
                posY <= (unit.getPosY() + 15) && posY >= (unit.getPosY() - 15)) {
                foundUnit = unit;
                //stop forEach
                return;
            }
        });
        return foundUnit;
    }


    public getFirstUnitInRadius(radius: number, posX: number, posY: number): Unit {
        //put all units in radius into a map together with their progress
        let unitsMap: { progress: number, unit: Unit }[] = new Array<{ progress: number, unit: Unit }>();
        for (let unit of this.units) {
            let differenceX: number;
            let differenceY: number;
            let distance: number;
            differenceX = unit.getPosX() - posX;
            differenceY = unit.getPosY() - posY;
            distance = Math.sqrt(Math.pow(differenceX, 2) + Math.pow(differenceY, 2));
            if (distance <= radius) {
                unitsMap.push({progress: unit.getProgress(), unit: unit });
            }
        }
        if (unitsMap.length == 0) {
            return null;
        } else {
            //sort the map and then return the first(highest progress) unit
            unitsMap.sort((a: { progress: number, unit: Unit }, b: { progress: number, unit: Unit }) => {
                return b.progress - a.progress;
            });
            return unitsMap[0].unit;
        }
    }

    public getAllUnitsInRadiusSorted(radius: number, posX: number, posY: number): Unit[] {
        //put all units in radius into a map together with their distance
        let unitsMap: { distance: number, unit: Unit }[] = new Array<{ distance: number, unit: Unit }>();
        for (let unit of this.units) {
            let differenceX: number;
            let differenceY: number;
            let distance: number;
            differenceX = unit.getPosX() - posX;
            differenceY = unit.getPosY() - posY;
            distance = Math.sqrt(Math.pow(differenceX, 2) + Math.pow(differenceY, 2));
            if (distance <= radius) {
                unitsMap.push({ distance: distance, unit: unit });
            }
        }
        //sort the map (distance ascending) and then return the units
        unitsMap.sort((a: { distance: number, unit: Unit }, b: { distance: number, unit: Unit }) => {
            return a.distance - b.distance;
        });
        return unitsMap.map((x) => { return x.unit });
    }

    public addUnit(unit: Unit): void {
        this.units.push(unit);
    }

    public removeUnit(unit: Unit): void {
        //removes element by filtering out all the others and assinging the result to the units array
        this.units = this.units.filter(Unit => Unit !== unit);
    }

    public addShot(shot: Shot): void {
        this.shots.push(shot);
    }

    public removeShot(shot: Shot): void {
        //removes element by filtering out all the others and assinging the result to the units array
        this.shots = this.shots.filter(Shot => Shot !== shot);
    }

    public getTowerAt(x: number, y: number): Tower {
        let tower: Tower = null;
        let gridPos: [number, number] = Constants.XYToGrid(x, y);
        this.towers.forEach((t) => {
            let towerGridPos: [number, number] = Constants.XYToGrid(t.getPosX(), t.getPosY());
            if (gridPos[0] == towerGridPos[0] && gridPos[1] == towerGridPos[1]) {
                tower = t;
            }
        });
        return tower;
    }

    //adds a Tower to the storage of the game
    public addTower(tower: Tower): void {
        this.towers.push(tower);
    }


    public placeTower(type: TowerType, posX: number, posY: number): void {
        //check for enough bits and space for the tower
        if (TowerCosts.getCostByType(type) <= this.bits) {
            let gridPos: [number, number] = Constants.XYToGrid(posX, posY);
            if (gridPos != null && this.grid[gridPos[1]][gridPos[0]] == 0 && type != TowerType.LinkTower) {
                let placeCoords: [number, number] = Constants.GridToXY(gridPos[0], gridPos[1]);
                let tower: Tower;
                switch (type) {
                    case TowerType.SniperTower:
                        tower = new SniperTower(this, placeCoords[0], placeCoords[1]);
                        this.bits -= TowerCosts.getCostByType(type);
                        break;
                    case TowerType.MGTower:
                        tower = new MGTower(this, placeCoords[0], placeCoords[1]);
                        this.bits -= TowerCosts.getCostByType(type);
                        break;
                    case TowerType.HomingTower:
                        tower = new HomingTower(this, placeCoords[0], placeCoords[1]);
                        this.bits -= TowerCosts.getCostByType(type);
                        break;
                    case TowerType.LightningTower:
                        tower = new LightningTower(this, placeCoords[0], placeCoords[1]);
                        this.bits -= TowerCosts.getCostByType(type);
                        break;
                    case TowerType.BombTower:
                        tower = new BombTower(this, placeCoords[0], placeCoords[1]);
                        this.bits -= TowerCosts.getCostByType(type);
                        break;
                    case TowerType.FrozenTower:
                        tower = new FrozenTower(this, placeCoords[0], placeCoords[1]);
                        this.bits -= TowerCosts.getCostByType(type);
                        break;
                }
                this.grid[gridPos[1]][gridPos[0]] = tower;
                this.stats.addTowerPlaced(1);
            } else if (gridPos != null && this.grid[gridPos[1]][gridPos[0]] == 2 && type == TowerType.LinkTower) {
                //the link tower is handled extra because it can only be placed on the track
                let placeCoords: [number, number] = Constants.GridToXY(gridPos[0], gridPos[1]);
                let tower: Tower;
                tower = new LinkTower(this, placeCoords[0], placeCoords[1]);
                this.bits -= TowerCosts.getCostByType(type);
                this.grid[gridPos[1]][gridPos[0]] = tower;
                this.stats.addTowerPlaced(1);
            }
        }
    }

    //removes the tower from the grid and the array
    public removeTower(tower: Tower): void {
        let gridPos: [number, number] = Constants.XYToGrid(tower.getPosX(), tower.getPosY());
        if (tower instanceof LinkTower) {
            this.grid[gridPos[1]][gridPos[0]] = 2;
        } else {
            this.grid[gridPos[1]][gridPos[0]] = 0;
        }
        this.towers = this.towers.filter(t => t !== tower);
    }

    //bits are capped at 999999
    public addBits(amount: number): void {
        this.bits += amount;
        this.stats.addBitsEarned(amount);
        if (this.bits > 999999) {
            this.bits = 999999;
        }
    }

    public pausePlay(): void {
        if (this.running) {
            this.running = false;
            this.stopLoop();
            this.drawer.draw();
        }
        else {
            this.running = true;
            this.startLoop();
        }
    }

    private end(): void {
        let element = document.getElementById("newGame");
        let canvas = document.getElementById("game-area");
        let rect = canvas.getBoundingClientRect();
        this.drawer.draw();
        this.drawer.drawGameOver();
        this.running = false;
        element.classList.remove("hidden");
        element.classList.add("active");
        document.getElementById("newGame").style.top = "" + (rect.top + 315).toString() + "px";
        element.style.left = "" + (rect.left + 288).toString() + "px";
    }

    public updatePositionNewGameButton(): void {
        let element = document.getElementById("newGame");
        let canvas = document.getElementById("game-area");
        let rect = canvas.getBoundingClientRect();
        if (!this.running) {
            element.classList.remove("hidden");
            element.classList.add("active");
            document.getElementById("newGame").style.top = "" + (rect.top + 315).toString() + "px";
            element.style.left = "" + (rect.left + 288).toString() + "px";
        }
    }

    public getCanvas(): HTMLCanvasElement {
        return this.canvas;
    }

    public getContext(): CanvasRenderingContext2D {
        return this.context;
    }

    public getTowers(): Tower[] {
        return this.towers;
    }

    public getShots(): Shot[] {
        return this.shots;
    }

    public getUnits(): Unit[] {
        return this.units;
    }

    public getProcessor(): Processor {
        return this.processor;
    }

    public getBackgroundImage(): HTMLImageElement {
        return this.backgroundImage;
    }

    public getBits(): number {
        return this.bits;
    }

    public getWave(): Wave {
        return this.wave;
    }

    public getDragNDrop(): MouseControl {
        return this.mouseControl;
    }

    public getStart(): [number, number] {
        return [this.startX, this.startY];
    }

    public getRunning(): boolean {
        return this.running;
    }
}