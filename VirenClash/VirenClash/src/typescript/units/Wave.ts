﻿/// <reference path="../references.ts" />

class Wave {

    private numberOfWave: number;
    private coolDown: number;
    private waveDuration: number;

    private totalNumberOfUnits: number;
    private healthFunctionSwitch: number;
    private activatedNumberOfUnits: number;
    //stores how many (from 0 to x) units are ready to spawn
    private unitBuildStatus: number;
    private averageNumberOfUnits: number;

    private game: Game
    private speedOfUnits: number;
    private healthOfUnits: number;

    constructor(game: Game) {
        this.reset();
        this.numberOfWave = 0;
        this.unitBuildStatus = 0;
        this.game = game;
        this.healthFunctionSwitch = 30;
    }

    public tick(): void {
        //normal tick (time between waves)
        if (this.coolDown > 1) {
            this.coolDown--;
        } else if (this.coolDown == 1) {
            //start new wave
            this.numberOfWave++;
            this.game.stats.addWaveReached(1);
            this.coolDown--;
            this.calculate();
        } else {
            //wave is finished
            if (this.waveDuration == 0) {
                this.reset();
                this.calculate();
            } else {
                //wave is in progress
                this.unitBuildStatus += this.averageNumberOfUnits;
                if (this.unitBuildStatus > 1 && this.waveDuration > 1) {
                    this.createUnits(Math.floor(this.unitBuildStatus));
                    this.unitBuildStatus -= Math.floor(this.unitBuildStatus);
                    this.waveDuration--;
                } else if (this.waveDuration == 1) {
                    this.createUnits(Math.floor(this.unitBuildStatus));
                    this.unitBuildStatus = 0;
                    this.waveDuration = 0;
                } else {
                    this.waveDuration--;
                }
            }
        }
    }

    private reset(): void {
        this.coolDown = 600;
        this.waveDuration = 600;
    }

    private createUnits(numberOfUnits: number): void {
        while (numberOfUnits >= 1) {
            let newUnit = new Unit(this.game, this.healthOfUnits, (1 + ((Math.random() / 4) - 0.125)) * this.speedOfUnits);
            numberOfUnits--;
        }
    }

    private calculate(): void {
        this.calculateNumberOfUnits();
        this.calculateHealthOfUnits();
        this.calculateSpeedOfUnits();
    }

    private calculateNumberOfUnits(): void {
        this.totalNumberOfUnits = this.numberOfWave + 10;

        //Raises or decreases the number of units from 0 to 10% randomly
        this.totalNumberOfUnits += ((Math.random() / 2) - 0.25) * this.totalNumberOfUnits;
        this.averageNumberOfUnits = this.totalNumberOfUnits / this.waveDuration;
    }

    private calculateSpeedOfUnits(): void {
        this.speedOfUnits = Math.sqrt(this.numberOfWave + 2);
    }

    //the health calculation is split into two different segements with different math functions to calculate it
    private calculateHealthOfUnits(): void {
        if (this.numberOfWave <= this.healthFunctionSwitch) {
            this.healthOfUnits = this.healthFunctionEarly();
        } else {
            this.healthOfUnits = this.healthFunctionLate();
        }
    }

    private healthFunctionEarly(): number {
        return 0.1 * this.numberOfWave * this.numberOfWave + 15;
    }

    private healthFunctionLate(): number {
        return 15 * Math.sqrt(this.numberOfWave - 20) + 50;
    }

    public getNumberOfWave(): number {
        return this.numberOfWave;
    }

    public getProgressOfCooldown(): number {
        return Math.floor((this.coolDown / 600) * 6);
    }
}