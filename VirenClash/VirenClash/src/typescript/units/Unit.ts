﻿/// <reference path="../references.ts" />

class Unit {

    private health: number;
    private maxHealth: number;
    private speed: number;
    private posX: number;
    private posY: number;
    private game: Game;
    //stores progress on map
    private progress: number;
    private slowEffects: SlowEffect[];

    constructor(game: Game, health: number, speed: number) {
        this.health = health;
        this.maxHealth = health;
        this.speed = speed;
        [this.posX, this.posY] = game.getStart();
        this.progress = 0;
        this.game = game;
        this.slowEffects = new Array<SlowEffect>();
        game.addUnit(this);
    }

    public tick(): void {
        this.updateSlowEffects();
        this.move();
    }

    private move(): void {
        this.progress += this.speed * this.calculateSlowingFactor();
        let newPos: [number, number] = this.game.getPosFromProgress(this.progress);
        if (newPos != null) {
            [this.posX, this.posY] = newPos;
        } else {
            this.game.removeUnit(this);
        }
    }

    public doDamage(amount: number): void {
        this.health -= amount;
        if (this.health <= 0) {
            this.game.addBits(Constants.unitValue);
            this.game.stats.addUnitKilled(1);
            this.game.removeUnit(this);
        }
    }

    public addSlowEffect(slowEffect: SlowEffect): void {
        let effectAdded = false;
        //updates an old effect from the same tower to the new one (when factor is (s)lower)
        for (var i = 0; i < this.slowEffects.length && !effectAdded; i++) {
            if (this.slowEffects[i].getSourceTower == slowEffect.getSourceTower) {
                if (this.slowEffects[i].getFactor() >= slowEffect.getFactor()) {
                    this.slowEffects[i] = slowEffect;
                    effectAdded = true;
                }
            }
        }
        if (!effectAdded) {
            this.slowEffects.push(slowEffect);
        }
    }

    //multiplies up all factor
    private calculateSlowingFactor(): number {
        let slowingFactor: number = 1;
        this.slowEffects.forEach((slowEffect) => {
            slowingFactor *= slowEffect.getFactor();
        });
        return slowingFactor;
    }

    //ticks all slow effects and removes the ones timed out
    private updateSlowEffects(): void {
        this.slowEffects.forEach((slowEffect) => {
            if (slowEffect.getDuration() > 0) {
                slowEffect.updateDuration();
            }
        });
        this.slowEffects = this.slowEffects.filter(SlowEffect => SlowEffect.getDuration() > 0);
    }

    public getMaxHealth(): number {
        return this.maxHealth;
    }

    public getHealth(): number {
        return this.health;
    }

    public getProgress(): number {
        return this.progress;
    }

    public getPosX(): number {
        return this.posX;
    }

    public getPosY(): number {
        return this.posY;
    }

    public getImageWithOpacity(): [HTMLImageElement, number] {
        let opacity: number = 0.5 + ((this.health / this.maxHealth) / 2);
        return [Pictures.unit, opacity];
    }
}