﻿/// <reference path="../references.ts" />

class SlowEffect {

    private sourceTower: FrozenSurface;
    private factor: number;
    private duration: number;

    constructor(sourceTower: FrozenSurface, factor: number, duration: number) {
        this.sourceTower = sourceTower;
        this.factor = factor;
        this.duration = duration;
    }

    public updateDuration(): void {
        this.duration--;
    }

    public getSourceTower(): FrozenSurface {
        return this.sourceTower;
    }

    public getFactor(): number {
        return this.factor;
    }

    public getDuration(): number {
        return this.duration;
    }
}