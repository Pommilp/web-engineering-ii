﻿/// <reference path="../references.ts" />

abstract class SingleTargetShot extends Shot {

    public hit(unit: Unit): void {
        unit.doDamage(this.damage);
        this.game.removeShot(this);
    }
}
