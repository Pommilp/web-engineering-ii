﻿/// <reference path="../references.ts" />

class Connection extends Shot {

    private maxHitUnits: number;
    private hitUnits: number;

    constructor(game: Game, posX: number, posY: number, maxHitUnits: number) {
        super(game, 0, posX, posY);
        this.maxHitUnits = maxHitUnits;
        this.hitUnits = 0;
        game.addShot(this);
    }

    //nothing has to be done in tick for connection
    //tick() is neccessary to iterate over all Shots 
    public tick(): void { }

    public hit(unit: Unit): void {
        unit.doDamage(unit.getMaxHealth());
        this.hitUnits++;

        //if the hit limit is exceeded remove the connection and the corresponding tower
        if (this.hitUnits >= this.maxHitUnits) {
            let tower = this.game.getTowerAt(this.posX, this.posY);
            if (tower != null) {
                this.game.removeTower(tower);
            }
            this.game.removeShot(this);
        }
    }

    public getImage(): [HTMLImageElement, number, number] {
        return [Pictures.connection, 1 - 0.7 * (this.hitUnits / this.maxHitUnits), 0];
    }
}