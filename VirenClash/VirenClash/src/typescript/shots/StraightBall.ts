﻿/// <reference path="../references.ts" />

class StraightBall extends SingleTargetShot {

    private distanceX: number;
    private distanceY: number;
    private moveX: number;
    private moveY: number;
    private bulletSpeed: number;
    protected angle: number;

    constructor(game: Game, targetX: number, targetY: number, damage: number, posX: number, posY: number, bulletSpeed: number, superCall: boolean = false) {
        super(game, damage, posX, posY);
        //convert the startpos and targetpos to a normalized vector (moveX, moveY) of the direction
        //moving with this vector is easier than the calculation with startpos and targetpos
        this.distanceX = targetX - posX;
        this.distanceY = targetY - posY;
        let normalizationFactor = Math.sqrt(Math.pow(this.distanceX, 2) + Math.pow(this.distanceY, 2));
        this.moveX = this.distanceX * bulletSpeed / normalizationFactor;
        this.moveY = this.distanceY * bulletSpeed / normalizationFactor;
        //calculates angle of bullet
        //atan2 has no definitiongaps
        this.angle = Math.atan2(this.moveY, this.moveX);
        this.bulletSpeed = bulletSpeed;
        //neccessary because snipershot extends this and the shot shouldn't be added two times
        if (!superCall) {
            game.addShot(this);
        }
    }

    public tick(): void {
        //remove if shot reached goal last tick
        if (this.distanceX == 0 && this.distanceY == 0) {
            this.game.removeShot(this);
        } else {
            this.move();
        }
    }

    private move(): void {
        //would move over target, so move only to target
        if (Math.abs(this.distanceX) < Math.abs(this.moveX)) {
            this.posX += this.distanceX;
            this.posY += this.distanceY;
            this.distanceX = 0;
            this.distanceY = 0;
        }
        //move normal distance
        else {
            this.posX += this.moveX;
            this.posY += this.moveY;
            this.distanceX -= this.moveX;
            this.distanceY -= this.moveY;
        }
    }

    public getImage(): [HTMLImageElement, number, number] {
        return [Pictures.straightShot, 1, this.angle];
    }

}