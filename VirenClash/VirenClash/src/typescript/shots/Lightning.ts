﻿/// <reference path="../references.ts" />

class Lightning extends Shot {

    private wayOfLightning: Unit[];
    private maxHits: number;
    private bounceRadius: number;
    private bounceTime: number;
    private bounceCooldown: number;
    private color: string;

    constructor(game: Game, target: Unit, damage: number, posX: number, posY: number, maxHits: number, bounceRadius: number, bounceTime: number, color: string) {
        super(game, damage, posX, posY);
        this.wayOfLightning = new Array<Unit>();
        this.wayOfLightning.push(target);
        this.maxHits = maxHits;
        this.bounceRadius = bounceRadius;
        this.bounceTime = bounceTime;
        this.bounceCooldown = bounceTime;
        this.color = color;
        this.game.addShot(this);
        target.doDamage(damage);
    }

    public tick(): void {
        if (this.bounceCooldown > 0) {
            this.bounceCooldown--;
            return;
        } else {
            //lightning can bounce (time)
            this.bounceCooldown = this.bounceTime;
            if (this.wayOfLightning.length < this.maxHits) {
                //lightning can bounce (bounce limit)
                let lastUnit: Unit = this.wayOfLightning[this.wayOfLightning.length - 1];
                let tipX: number = lastUnit.getPosX();
                let tipY: number = lastUnit.getPosY();
                var possibleTargets: Unit[] = this.game.getAllUnitsInRadiusSorted(this.bounceRadius, tipX, tipY);

                //scans possibleTargets for units not yet affected by this ligthning
                if (possibleTargets.length > 0) {
                    let hit: boolean = false;
                    for (let i = 0; i < possibleTargets.length; i++) {
                        if (this.wayOfLightning.filter(u => u == possibleTargets[i]).length == 0) {
                            this.wayOfLightning.push(possibleTargets[i]);
                            possibleTargets[i].doDamage(this.damage);
                            hit = true;
                            return;
                        }
                    }
                    //could not hit a new unit (all units in range are already hit)
                    if (!hit) {
                        this.game.removeShot(this);
                    }
                } else {
                    //could not hit a new unit (no unit in range)
                    this.game.removeShot(this);
                }
                //already max amount of units hit
            } else {
                this.game.removeShot(this);
            }
        }

    }

    //hit is not used for ligthning, damage will be done in move
    //hit function is neccesary because itaration through all shot isn't possible without
    public hit(unit: Unit): void { }

    public getImage(): [HTMLImageElement, number, number] {
        return null;
    }

    public getColor(): string {
        return this.color;
    }

    public getWayOfLightning(): Unit[] {
        return this.wayOfLightning;
    }
}