﻿/// <reference path="../references.ts" />

abstract class Shot {

    protected damage: number;
    protected posX: number;
    protected posY: number;
    protected game: Game;

    constructor(game: Game, damage: number, posX: number, posY: number) {
        this.damage = damage;
        this.posX = posX;
        this.posY = posY;
        this.game = game;
    }

    public abstract tick(): void;
    public abstract hit(unit: Unit): void;

    public getPosX(): number {
        return this.posX;
    }

    public getPosY(): number {
        return this.posY;
    }

    //returns image, opacity, angle
    public abstract getImage(): [HTMLImageElement, number, number];
}