﻿/// <reference path="../references.ts" />

class DirectedBall extends SingleTargetShot {
    private target: Unit;
    private bulletSpeed: number;
    private angle: number;

    constructor(game: Game, target: Unit, damage: number, posX: number, posY: number, bulletSpeed: number) {
        super(game, damage, posX, posY);
        
        this.target = target;
        this.bulletSpeed = bulletSpeed;
        game.addShot(this);
    }

    public tick(): void {
        this.move();
    }

    private move(): void {
        //calculates the distance that has to be moved in x and y
        let x: number = this.target.getPosX() - this.posX;
        let y: number = this.target.getPosY() - this.posY;
        let normalizationFactor = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
        let moveX = x * this.bulletSpeed / normalizationFactor;
        let moveY = y * this.bulletSpeed / normalizationFactor;
        //calculates angle of bullet
        //atan2 has no definitiongaps
        this.angle = Math.atan2(moveY, moveX);
        //shot would move over unit, so it is moved on th position of the unit
        if (Math.abs(moveX) > Math.abs(x) || Math.abs(moveY) > Math.abs(y)) {
            this.posX = this.target.getPosX();
            this.posY = this.target.getPosY();
        } else {
            //normal moving
            this.posX += moveX;
            this.posY += moveY;
        }
    }

    public getImage(): [HTMLImageElement, number, number] {
        return [Pictures.directedShot, 1, this.angle];
    }
}