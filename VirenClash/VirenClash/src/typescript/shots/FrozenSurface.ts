﻿/// <reference path="../references.ts" />

class FrozenSurface extends Shot {

    private frozenPeriod: number;
    private maxFrozenTime: number;
    private radius: number;

    constructor(game: Game, posX: number, posY: number, frozenPeriod: number, radius: number) {
        super(game, 0, posX, posY);
        this.frozenPeriod = frozenPeriod;
        this.maxFrozenTime = frozenPeriod;
        this.radius = radius;
        this.freeze();
        game.addShot(this);
    }

    public tick(): void {
        if (this.frozenPeriod > 0) {
            this.frozenPeriod--;
            this.slow();
        } else {
            this.game.removeShot(this);
        }
    }

    //adds a slow effect to all units in range
    private slow(): void {
        let units: Unit[] = this.game.getAllUnitsInRadiusSorted(this.radius, this.posX, this.posY);
        let slowEffect: SlowEffect = new SlowEffect(this, Balance.FrozenSFCoolFactor, Balance.FrozenSFCoolTime);
        units.forEach((unit) => {
            unit.addSlowEffect(slowEffect);
        });
    }

    //freezes all unit in range
    private freeze(): void {
        let units: Unit[];
        units = this.game.getAllUnitsInRadiusSorted(this.radius, this.posX, this.posY);
        units.forEach((unit) => {
            let slowEffect: SlowEffect = new SlowEffect(this, Balance.FrozenSFFreezeFactor, Balance.FrozenSFFreezeTime);
            unit.addSlowEffect(slowEffect);
        });
    }

    //hit is not used for FrozenSurface, slow will be done in tick()
    //hit function is neccesary because itaration through all shot isn't possible without
    public hit(): void { }

    public getImage(): [HTMLImageElement, number, number] {
        let opacity: number = 0.2 + (this.frozenPeriod / this.maxFrozenTime) * 0.8;
        return [Pictures.frozenSurface, opacity, 0];
    }
}