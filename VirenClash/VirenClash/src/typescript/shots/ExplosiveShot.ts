﻿/// <reference path="../references.ts" />

class ExplosiveShot extends Shot {

    private distanceX: number;
    private distanceY: number;
    private moveX: number;
    private moveY: number;
    private radius: number;
    private bulletSpeed: number;
    private burnDamage: number;
    private exploded: boolean;
    private burnedTime: number;
    private maxBurnTime: number;
    private angle: number;

    constructor(game: Game, targetX: number, targetY: number, posX: number, posY: number, radius: number, bulletSpeed: number, explosionDamage: number, burnDamage: number, burnTime: number) {
        super(game, explosionDamage, posX, posY);
        //convert the startpos and targetpos to a normalized vector (moveX, moveY) of the direction
        //moving with this vector is easier than the calculation with startpos and targetpos
        this.distanceX = targetX - posX;
        this.distanceY = targetY - posY;
        let normalizationFactor = Math.sqrt(Math.pow(this.distanceX, 2) + Math.pow(this.distanceY, 2));
        this.moveX = this.distanceX * bulletSpeed / normalizationFactor;
        this.moveY = this.distanceY * bulletSpeed / normalizationFactor;
        //calculates angle of bullet
        //atan2 has no definitiongaps
        this.angle = Math.atan2(this.moveY, this.moveX);
        this.radius = radius;
        this.bulletSpeed = bulletSpeed;
        this.burnDamage = burnDamage;
        this.exploded = false;
        this.maxBurnTime = burnTime;
        this.burnedTime = 0;
        game.addShot(this);
    }

    public tick(): void {
        if (!this.exploded) {
            this.move();
        } else {
            this.burn();
        }
    }

    //do the explosion (damage to all units in range)
    public hit(): void {
        if (!this.exploded) {
            this.exploded = true;
            let targets: Unit[] = this.game.getAllUnitsInRadiusSorted(this.radius, this.posX, this.posY);
            targets.forEach((unit) => {
                unit.doDamage(this.damage);
            });
        }
    }

    private move(): void {
        //would move over target, so move only to target
        if (Math.abs(this.distanceX) < Math.abs(this.moveX)) {
            this.posX += this.distanceX;
            this.posY += this.distanceY;
            this.distanceX = 0;
            this.distanceY = 0;
            this.hit();
        }
        else {
            this.posX += this.moveX;
            this.posY += this.moveY;
            this.distanceX -= this.moveX;
            this.distanceY -= this.moveY;
        }
    }

    //does burn damage to all units in range
    //removes shot if burning ends
    private burn(): void {
        if (this.burnedTime < this.maxBurnTime) {
            this.burnedTime++;
            let targets: Unit[] = this.game.getAllUnitsInRadiusSorted(this.radius, this.posX, this.posY);
            targets.forEach((unit) => {
                unit.doDamage(this.burnDamage);
            });
        } else {
            this.game.removeShot(this);
        }
    }

    //burning is animated, therefore multiple pictures
    public getImage(): [HTMLImageElement, number, number] {
        if (this.exploded) {
            let factor: number;
            factor = this.burnedTime / this.maxBurnTime;
            if (factor > 0.9) {
                return [Pictures.explosiveBurning1, 0.5, 0];
            }
            else if (factor > 0.8) {
                return [Pictures.explosiveBurning2, 0.5, 0];
            }
            else if (factor > 0.7) {
                return [Pictures.explosiveBurning3, 0.5, 0];
            }
            else if (factor > 0.6) {
                return [Pictures.explosiveBurning4, 0.5, 0];
            }
            else if (factor > 0.5) {
                return [Pictures.explosiveBurning5, 0.5, 0];
            }
            else if (factor > 0.4) {
                return [Pictures.explosiveBurning6, 0.5, 0];
            }
            else if (factor > 0.3) {
                return [Pictures.explosiveBurning7, 0.5, 0];
            }
            else if (factor > 0.2) {
                return [Pictures.explosiveBurning8, 0.5, 0];
            }
            else if (factor > 0.1) {
                return [Pictures.explosiveBurning9, 0.5, 0];
            }
            else {
                return [Pictures.explosiveBurning10, 0.5, 0];
            }
        } else {
            return [Pictures.explosiveShot, 1, this.angle];
        }
    }
}