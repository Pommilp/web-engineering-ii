﻿/// <reference path="../references.ts" />

class SniperShot extends StraightBall {

    private damageFactor: number;

    constructor(game: Game, targetX: number, targetY: number, damage: number, damageFactor:number, posX: number, posY: number, bulletSpeed: number) {
        super(game, targetX, targetY, damage, posX, posY, bulletSpeed, true);
        this.damageFactor = damageFactor;
        game.addShot(this);
    }


    public hit(unit: Unit): void {
        //units health is lower or equal to the bullet damage --> do this damage
        if (unit.getHealth() <= this.damage) {
            unit.doDamage(this.damage);
        } else {
            //unit has more health than the bullet damage --> do procentual damage to the units health
            unit.doDamage(unit.getHealth() * this.damageFactor);
        }
        this.game.removeShot(this);
    }

    public getImage(): [HTMLImageElement, number, number] {
        return [Pictures.sniperShot, 1, this.angle];
    }
}