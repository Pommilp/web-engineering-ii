﻿/// <reference path="../references.ts" />

class Pictures {

    //this class stores all the picturs everywhere else is a reference (done indirect) used
    //saves a lot of storage

    public static sniperTower: HTMLImageElement;
    public static sniperTowerDND: HTMLImageElement;
    public static mGTower: HTMLImageElement;
    public static mGTowerDND: HTMLImageElement;
    public static homingTower: HTMLImageElement;
    public static homingTowerDND: HTMLImageElement;
    public static lightningTower: HTMLImageElement;
    public static lightningTowerDND: HTMLImageElement;
    public static bombTower: HTMLImageElement;
    public static bombTowerDND: HTMLImageElement;
    public static frozenTower: HTMLImageElement;
    public static frozenTowerDND: HTMLImageElement;
    public static straightShot: HTMLImageElement;
    public static directedShot: HTMLImageElement;
    public static explosiveShot: HTMLImageElement;
    public static sniperShot: HTMLImageElement;
    public static explosiveBurning1: HTMLImageElement;
    public static explosiveBurning2: HTMLImageElement;
    public static explosiveBurning3: HTMLImageElement;
    public static explosiveBurning4: HTMLImageElement;
    public static explosiveBurning5: HTMLImageElement;
    public static explosiveBurning6: HTMLImageElement;
    public static explosiveBurning7: HTMLImageElement;
    public static explosiveBurning8: HTMLImageElement;
    public static explosiveBurning9: HTMLImageElement;
    public static explosiveBurning10: HTMLImageElement;
    public static frozenSurface: HTMLImageElement;
    public static connection: HTMLImageElement;
    public static unit: HTMLImageElement;
    public static processor: HTMLImageElement;
    public static processorDestroyed: HTMLImageElement;
    public static rightMenu: HTMLImageElement;
    public static gameover: HTMLImageElement;
    public static play: HTMLImageElement;
    public static pause: HTMLImageElement
    public static SSeg0: HTMLImageElement;
    public static SSeg1: HTMLImageElement;
    public static SSeg2: HTMLImageElement;
    public static SSeg3: HTMLImageElement;
    public static SSeg4: HTMLImageElement;
    public static SSeg5: HTMLImageElement;
    public static SSeg6: HTMLImageElement;
    public static SSeg7: HTMLImageElement;
    public static SSeg8: HTMLImageElement;
    public static SSeg9: HTMLImageElement;
    public static ledOn: HTMLImageElement;
    public static ledOff: HTMLImageElement;
    public static world1: HTMLImageElement;

    public static init(): void {
        this.sniperTower = new Image();
        this.sniperTower.src = "/images/towers/sniperTower_stufe1.png";
        this.sniperTowerDND = new Image();
        this.sniperTowerDND.src = "/images/towers_circle/sniperTower_circle.png";
        this.mGTower = new Image();
        this.mGTower.src = "/images/towers/mGTower_stufe1.png";
        this.mGTowerDND = new Image();
        this.mGTowerDND.src = "/images/towers_circle/mGTower_circle.png";
        this.homingTower = new Image();
        this.homingTower.src = "/images/towers/homingTower_stufe1.png";
        this.homingTowerDND = new Image();
        this.homingTowerDND.src = "/images/towers_circle/homingTower_circle.png";
        this.lightningTower = new Image();
        this.lightningTower.src = "/images/towers/lightningTower_stufe1.png";
        this.lightningTowerDND = new Image();
        this.lightningTowerDND.src = "/images/towers_circle/lightningTower_circle.png";
        this.bombTower = new Image();
        this.bombTower.src = "/images/towers/bombTower_stufe1.png";
        this.bombTowerDND = new Image();
        this.bombTowerDND.src = "/images/towers_circle/bombTower_circle.png";
        this.frozenTower = new Image();
        this.frozenTower.src = "/images/towers/frozenTower_stufe1.png";
        this.frozenTowerDND = new Image();
        this.frozenTowerDND.src = "/images/towers_circle/frozenTower_circle.png";
        this.straightShot = new Image();
        this.straightShot.src = "/images/mgShot.png";
        this.directedShot = new Image();
        this.directedShot.src = "/images/directedShot.png";
        this.explosiveShot = new Image();
        this.explosiveShot.src = "/images/bomb.png";
        this.sniperShot = new Image();
        this.sniperShot.src = "/images/sniperShot.png";
        this.explosiveBurning1 = new Image();
        this.explosiveBurning1.src = "/images/explosion1.png";
        this.explosiveBurning2 = new Image();
        this.explosiveBurning2.src = "/images/explosion2.png";
        this.explosiveBurning3 = new Image();
        this.explosiveBurning3.src = "/images/explosion3.png";
        this.explosiveBurning4 = new Image();
        this.explosiveBurning4.src = "/images/explosion4.png";
        this.explosiveBurning5 = new Image();
        this.explosiveBurning5.src = "/images/explosion5.png";
        this.explosiveBurning6 = new Image();
        this.explosiveBurning6.src = "/images/explosion6.png";
        this.explosiveBurning7 = new Image();
        this.explosiveBurning7.src = "/images/explosion7.png";
        this.explosiveBurning8 = new Image();
        this.explosiveBurning8.src = "/images/explosion8.png";
        this.explosiveBurning9 = new Image();
        this.explosiveBurning9.src = "/images/explosion9.png";
        this.explosiveBurning10 = new Image();
        this.explosiveBurning10.src = "/images/explosion10.png";
        this.frozenSurface = new Image();
        this.frozenSurface.src = "/images/frozenSurface.png";
        this.connection = new Image();
        this.connection.src = "/images/connection.png";
        this.unit = new Image();
        this.unit.src = "/images/unit.png";
        this.processor = new Image();
        this.processor.src = "/images/prozessor.png";
        this.processorDestroyed = new Image();
        this.processorDestroyed.src = "/images/prozessor_kaputt.png";
        this.rightMenu = new Image();
        this.rightMenu.src = "/images/right_menu.png";
        this.gameover = new Image();
        this.gameover.src = "/images/gameover.png";
        this.pause = new Image();
        this.pause.src = "/images/pause.png";
        this.play = new Image();
        this.play.src = "/images/play.png";
        this.SSeg0 = new Image();
        this.SSeg0.src = "/images/SSeg/SSeg0.png";
        this.SSeg1 = new Image();
        this.SSeg1.src = "/images/SSeg/SSeg1.png";
        this.SSeg2 = new Image();
        this.SSeg2.src = "/images/SSeg/SSeg2.png";
        this.SSeg3 = new Image();
        this.SSeg3.src = "/images/SSeg/SSeg3.png";
        this.SSeg4 = new Image();
        this.SSeg4.src = "/images/SSeg/SSeg4.png";
        this.SSeg5 = new Image();
        this.SSeg5.src = "/images/SSeg/SSeg5.png";
        this.SSeg6 = new Image();
        this.SSeg6.src = "/images/SSeg/SSeg6.png";
        this.SSeg7 = new Image();
        this.SSeg7.src = "/images/SSeg/SSeg7.png";
        this.SSeg8 = new Image();
        this.SSeg8.src = "/images/SSeg/SSeg8.png";
        this.SSeg9 = new Image();
        this.SSeg9.src = "/images/SSeg/SSeg9.png";
        this.ledOn = new Image();
        this.ledOn.src = "/images/LED_an.png";
        this.ledOff = new Image();
        this.ledOff.src = "/images/LED_aus.png";
        this.world1 = new Image();
        this.world1.src = "/images/world1.png";
    }

    public static getSSegByDigit(digit: number) {
        switch (digit) {
            case 1:
                return this.SSeg1;
            case 2:
                return this.SSeg2;
            case 3:
                return this.SSeg3;
            case 4:
                return this.SSeg4;
            case 5:
                return this.SSeg5;
            case 6:
                return this.SSeg6;
            case 7:
                return this.SSeg7;
            case 8:
                return this.SSeg8;
            case 9:
                return this.SSeg9;
            default:
                return this.SSeg0;
        }
    }


}