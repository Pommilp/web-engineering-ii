﻿/// <reference path="../references.ts" />

class Drawer {

    private game: Game;

    constructor(game: Game) {
        this.game = game;
    }

    //draws a picture on the canvas 
    private drawPicture(picture: HTMLImageElement, x: number, y: number, alpha: number = 1, rotation: number = 0) {
        let posX = 0;
        let posY = 0;

        //draw with rotation if set
        if (rotation != 0) {
            //rotation is done through a rotation around the picture middle, then the picture is drawn and afterwards the rotation is undone (restore)
            posX = -(picture.width / 2);
            posY = -(picture.height / 2);
            this.game.getContext().save();
            this.game.getContext().translate(x, y);
            this.game.getContext().rotate(rotation);
        } else {
            posX = x - (picture.width / 2);
            posY = y - (picture.height /2);
        }
        this.game.getContext().globalAlpha = alpha;
        this.game.getContext().drawImage(picture, posX, posY);
        this.game.getContext().globalAlpha = 1;
        if (rotation != 0) {
            this.game.getContext().restore();
        }
    }

    public draw() {
        //clear canvas
        this.game.getContext().clearRect(0, 0, this.game.getCanvas().width, this.game.getCanvas().height);
        this.game.getContext().drawImage(this.game.getBackgroundImage(), 0, 0);

        this.game.getTowers().forEach((tower) => {
            this.drawTower(tower);
        });

        //explosive and forzen are drawn below (before) the units
        this.game.getShots().forEach((shot) => {
            if ((shot instanceof ExplosiveShot) || (shot instanceof FrozenSurface)) {
                this.drawShot(shot);
            }
        });

        this.game.getUnits().forEach((unit) => {
            this.drawUnit(unit);
        });

        this.drawProcessor();

        //draw the rest of the shots
        this.game.getShots().forEach((shot) => {
            if (shot instanceof Lightning) {
                this.drawLigthning(shot);
            }
            else if (!(shot instanceof ExplosiveShot) && !(shot instanceof FrozenSurface)) {
                this.drawShot(shot);
            }

        });

        this.game.getContext().drawImage(Pictures.rightMenu, 21 * Constants.gridSize, 2 * Constants.gridSize);

        //this.game.getContext().drawImage(Pictures.browser, 9 * Constants.gridSize, 0);

        //draw bits
        this.draw7SegmentDisplay(18.5, 0.5, this.game.getBits());

        //draw number of wave
        this.draw7SegmentDisplay(0.5, 0.5, this.game.getWave().getNumberOfWave());

        this.drawCooldownUntilNextWave();

        this.drawPausePlay();

        this.drawDragNDrop();
    }


    private drawUnit(unit: Unit): void {
        let image: HTMLImageElement;
        let opacity: number;
        [image, opacity] = unit.getImageWithOpacity();
        this.drawPicture(image, unit.getPosX(), unit.getPosY(), opacity);
    }

    private drawShot(shot: Shot): void {
        let image: HTMLImageElement;
        let opacity: number;
        let angle: number;
        [image, opacity, angle] = shot.getImage();
        this.drawPicture(image, shot.getPosX(), shot.getPosY(), opacity, angle);
    }

    private drawTower(tower: Tower): void {
        let image: HTMLImageElement = tower.getImage();
        if (image != null) {
            this.drawPicture(image, tower.getPosX(), tower.getPosY());
        }
    }

    //a lightning is drawn line by line from one unit to the next
    private drawLigthning(lightning: Lightning): void {
        const targets: Unit[] = lightning.getWayOfLightning();
        let color: string = lightning.getColor();

        this.drawLightningLine(lightning.getPosX(), lightning.getPosY(), targets[0].getPosX(), targets[0].getPosY(), color);

        for (var i = 1; i < targets.length; i++) {
            this.drawLightningLine(targets[i - 1].getPosX(), targets[i - 1].getPosY(), targets[i].getPosX(), targets[i].getPosY(), color);
        }
    }

    //a line of a lightning is drawn by two overlaying normal canvas lines
    private drawLightningLine(fromX: number, fromY: number, toX: number, toY: number, color: string): void {
        this.game.getContext().beginPath();
        this.game.getContext().moveTo(fromX, fromY);
        this.game.getContext().lineTo(toX, toY);
        this.game.getContext().lineWidth = 3;
        this.game.getContext().strokeStyle = '#FFFFFF';
        this.game.getContext().stroke();
        this.game.getContext().beginPath();
        this.game.getContext().moveTo(fromX, fromY);
        this.game.getContext().lineTo(toX, toY);
        this.game.getContext().lineWidth = 2;
        this.game.getContext().strokeStyle = color;
        this.game.getContext().stroke();
    }

    private drawProcessor(): void {
        let image: HTMLImageElement;
        let imageDestroyed: HTMLImageElement;
        let opacity: number;
        [image, imageDestroyed, opacity] = this.game.getProcessor().getImages();
        this.drawPicture(image, Constants.prozessorPos[0], Constants.prozessorPos[1]);
        this.drawPicture(imageDestroyed, Constants.prozessorPos[0], Constants.prozessorPos[1], 1 - opacity);
    }

    private draw7SegmentDisplay(startX: number, startY: number, displayValue: number) {
        let digitString: string[] = (displayValue + '').split('');
        let digits: number[] = digitString.map(Number);
        let drawingPosX: number = startX;
        //0.(0-5)
        //draw digits not set by stored value to 0
        for (let i = 1; i <= (6 - digits.length); i++) {
            this.drawPicture(Pictures.SSeg0, drawingPosX * Constants.gridSize, startY * Constants.gridSize);
            drawingPosX++;
        }
        for (let i of digits) {
            this.drawPicture(Pictures.getSSegByDigit(i), drawingPosX * Constants.gridSize, startY * Constants.gridSize);
            drawingPosX++;
        }
    }

    private drawCooldownUntilNextWave(): void {
        let progressOfCooldown: number = this.game.getWave().getProgressOfCooldown();
        let drawingPosX: number = 0.5;
        let drawingPosY: number = 1.5;
        for (let i = 1; i <= progressOfCooldown; i++) {
            this.drawPicture(Pictures.ledOn, drawingPosX * Constants.gridSize, drawingPosY * Constants.gridSize);
            drawingPosX++;
        }
        for (let i = 1; i <= (6 - progressOfCooldown); i++) {
            this.drawPicture(Pictures.ledOff, drawingPosX * Constants.gridSize, drawingPosY * Constants.gridSize);
            drawingPosX++;
        }
    }

    private drawDragNDrop(): void {
        let image = this.game.getDragNDrop().getDrawImage();
        if (image != null) {
            let [x, y] = this.game.getDragNDrop().getDrawCords();
            this.drawPicture(image, x, y);
        }
    }

    private drawPausePlay() {
        if (this.game.getRunning()) {
            this.drawPicture(Pictures.pause, 15.5 * Constants.gridSize, 1 * Constants.gridSize);
        }
        else {
            this.drawPicture(Pictures.play, 15.5 * Constants.gridSize, 1 * Constants.gridSize);
        }
    }

    //numbers over 999999999 are converted to exponential
    public drawGameOver() {
        this.game.getContext().drawImage(Pictures.gameover, 0, 0);
        this.game.getContext().font = "15px Courier New";
        let text: string;
        let waveReached: number;
        let towersPlaced: number;
        let unitsKilled: number;
        let bitsEarned: number;
        [waveReached, towersPlaced, unitsKilled, bitsEarned] = this.game.stats.getStats();
        text = "Erreichte Welle:   ";
        if (waveReached > 999999999) {
            text += waveReached.toExponential(2);
        }
        else {
            text += waveReached;
        }
        this.game.getContext().fillText(text, 220, 220);
        text = "Vernichtete Viren: ";
        if (unitsKilled > 999999999) {
            text += unitsKilled.toExponential(2);
        }
        else {
            text += unitsKilled;
        }
        this.game.getContext().fillText(text, 220, 245);
        text = "Erhaltene Bits:    ";
        if (bitsEarned > 999999999) {
            text += bitsEarned.toExponential(2);
        }
        else {
            text += bitsEarned;
        }
        this.game.getContext().fillText(text, 220, 270);
        text = "Anzahl der Tower:  " + towersPlaced;
        this.game.getContext().fillText(text, 220, 295);
    }
}