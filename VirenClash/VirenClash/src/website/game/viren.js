var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/// <reference path="../references.ts" />
var Game = /** @class */ (function () {
    function Game(backgroundImage, startX, startY, track, grid, processorHealth) {
        //this var is used to safe a link to the requestAnimationFrame Loop, to be able to destroy it at stopLoop()
        this.loopStorage = undefined;
        this.bits = Constants.startingBits;
        this.backgroundImage = backgroundImage;
        this.startX = startX * Constants.gridSize;
        this.startY = startY * Constants.gridSize;
        this.canvas = document.getElementById("game-area");
        this.context = this.canvas.getContext("2d");
        this.units = new Array();
        this.towers = new Array();
        this.track = track;
        Constants.convertTrack(this.track);
        this.shots = new Array();
        this.grid = grid;
        this.mouseControl = new MouseControl(this, this.canvas);
        Pictures.init();
        this.processor = new Processor(processorHealth);
        this.wave = new Wave(this);
        this.stats = new Stats();
        this.drawer = new Drawer(this);
        this.running = true;
        window.onresize = this.updatePositionNewGameButton.bind(this);
    }
    Game.prototype.startLoop = function () {
        if (this.loopStorage == undefined) {
            this.loopStorage = window.requestAnimationFrame(this.gameLoop.bind(this));
        }
    };
    Game.prototype.stopLoop = function () {
        window.cancelAnimationFrame(this.loopStorage);
        this.loopStorage = undefined;
    };
    //here is what happens in the game
    Game.prototype.gameLoop = function () {
        //this is used to slow the gameloop for debug purposes
        /*this.debugSlow++;
        if (this.debugSlow < 1) {
            this.loopStorage = window.requestAnimationFrame(this.gameLoop.bind(this));
            return;
        }
        this.debugSlow = 0;*/
        //actual gameloop code
        this.wave.tick();
        this.tickTowers();
        this.tickUnits();
        this.tickShots();
        this.checkHits();
        if (this.processor.getHealthPercentage() <= 0) {
            this.end();
            //end animation loop
            return;
        }
        this.drawer.draw();
        this.loopStorage = window.requestAnimationFrame(this.gameLoop.bind(this));
    };
    //calls tick on all shots and removes the ones out of the canvas
    Game.prototype.tickShots = function () {
        var _this = this;
        this.shots.forEach(function (shot) {
            shot.tick();
            var x = shot.getPosX();
            var y = shot.getPosY();
            if (x < 0 || y < 0 || x > _this.canvas.width || y > _this.canvas.width) {
                _this.removeShot(shot);
            }
        });
    };
    //calls tick on all units and removes the ones out of the canvas
    Game.prototype.tickUnits = function () {
        var _this = this;
        this.units.forEach(function (unit) {
            unit.tick();
            var x = unit.getPosX();
            var y = unit.getPosY();
            if (x < 0 || y < 0 || x > _this.canvas.width || y > _this.canvas.height) {
                _this.removeUnit(unit);
            }
        });
    };
    Game.prototype.tickTowers = function () {
        this.towers.forEach(function (tower) {
            tower.tick();
        });
    };
    //checks for each shot if there is a unit to hit
    Game.prototype.checkHits = function () {
        var _this = this;
        this.shots.forEach(function (shot) {
            var foundUnit = _this.getUnitAt(shot.getPosX(), shot.getPosY());
            if (foundUnit != null) {
                shot.hit(foundUnit);
            }
        });
    };
    //walks through the track sectionwise until the given progress is reached
    Game.prototype.getPosFromProgress = function (prog) {
        var newPos = this.getStart();
        var move = prog;
        for (var i = 0; i < this.track.length; i++) {
            var steps = Math.abs(this.track[i][0] + this.track[i][1]);
            if (move >= steps) {
                newPos[0] += this.track[i][0];
                newPos[1] += this.track[i][1];
                move -= steps;
            }
            else {
                if (this.track[i][0] != 0) {
                    if (this.track[i][0] > 0) {
                        newPos[0] += move;
                    }
                    else {
                        newPos[0] -= move;
                    }
                }
                else {
                    if (this.track[i][1] > 0) {
                        newPos[1] += move;
                    }
                    else {
                        newPos[1] -= move;
                    }
                }
                move = 0;
                i = this.track.length;
            }
        }
        if (move > 0) {
            //unit reached the end of the track
            this.processor.doDamage(Constants.unitDamage);
            return null;
        }
        return newPos;
    };
    //returns the first unit in the units array at the given position
    Game.prototype.getUnitAt = function (posX, posY) {
        var foundUnit = null;
        this.units.forEach(function (unit) {
            if (posX <= (unit.getPosX() + 15) && posX >= (unit.getPosX() - 15) &&
                posY <= (unit.getPosY() + 15) && posY >= (unit.getPosY() - 15)) {
                foundUnit = unit;
                //stop forEach
                return;
            }
        });
        return foundUnit;
    };
    Game.prototype.getFirstUnitInRadius = function (radius, posX, posY) {
        //put all units in radius into a map together with their progress
        var unitsMap = new Array();
        for (var _i = 0, _a = this.units; _i < _a.length; _i++) {
            var unit = _a[_i];
            var differenceX = void 0;
            var differenceY = void 0;
            var distance = void 0;
            differenceX = unit.getPosX() - posX;
            differenceY = unit.getPosY() - posY;
            distance = Math.sqrt(Math.pow(differenceX, 2) + Math.pow(differenceY, 2));
            if (distance <= radius) {
                unitsMap.push({ progress: unit.getProgress(), unit: unit });
            }
        }
        if (unitsMap.length == 0) {
            return null;
        }
        else {
            //sort the map and then return the first(highest progress) unit
            unitsMap.sort(function (a, b) {
                return b.progress - a.progress;
            });
            return unitsMap[0].unit;
        }
    };
    Game.prototype.getAllUnitsInRadiusSorted = function (radius, posX, posY) {
        //put all units in radius into a map together with their distance
        var unitsMap = new Array();
        for (var _i = 0, _a = this.units; _i < _a.length; _i++) {
            var unit = _a[_i];
            var differenceX = void 0;
            var differenceY = void 0;
            var distance = void 0;
            differenceX = unit.getPosX() - posX;
            differenceY = unit.getPosY() - posY;
            distance = Math.sqrt(Math.pow(differenceX, 2) + Math.pow(differenceY, 2));
            if (distance <= radius) {
                unitsMap.push({ distance: distance, unit: unit });
            }
        }
        //sort the map (distance ascending) and then return the units
        unitsMap.sort(function (a, b) {
            return a.distance - b.distance;
        });
        return unitsMap.map(function (x) { return x.unit; });
    };
    Game.prototype.addUnit = function (unit) {
        this.units.push(unit);
    };
    Game.prototype.removeUnit = function (unit) {
        //removes element by filtering out all the others and assinging the result to the units array
        this.units = this.units.filter(function (Unit) { return Unit !== unit; });
    };
    Game.prototype.addShot = function (shot) {
        this.shots.push(shot);
    };
    Game.prototype.removeShot = function (shot) {
        //removes element by filtering out all the others and assinging the result to the units array
        this.shots = this.shots.filter(function (Shot) { return Shot !== shot; });
    };
    Game.prototype.getTowerAt = function (x, y) {
        var tower = null;
        var gridPos = Constants.XYToGrid(x, y);
        this.towers.forEach(function (t) {
            var towerGridPos = Constants.XYToGrid(t.getPosX(), t.getPosY());
            if (gridPos[0] == towerGridPos[0] && gridPos[1] == towerGridPos[1]) {
                tower = t;
            }
        });
        return tower;
    };
    //adds a Tower to the storage of the game
    Game.prototype.addTower = function (tower) {
        this.towers.push(tower);
    };
    Game.prototype.placeTower = function (type, posX, posY) {
        //check for enough bits and space for the tower
        if (TowerCosts.getCostByType(type) <= this.bits) {
            var gridPos = Constants.XYToGrid(posX, posY);
            if (gridPos != null && this.grid[gridPos[1]][gridPos[0]] == 0 && type != TowerType.LinkTower) {
                var placeCoords = Constants.GridToXY(gridPos[0], gridPos[1]);
                var tower = void 0;
                switch (type) {
                    case TowerType.SniperTower:
                        tower = new SniperTower(this, placeCoords[0], placeCoords[1]);
                        this.bits -= TowerCosts.getCostByType(type);
                        break;
                    case TowerType.MGTower:
                        tower = new MGTower(this, placeCoords[0], placeCoords[1]);
                        this.bits -= TowerCosts.getCostByType(type);
                        break;
                    case TowerType.HomingTower:
                        tower = new HomingTower(this, placeCoords[0], placeCoords[1]);
                        this.bits -= TowerCosts.getCostByType(type);
                        break;
                    case TowerType.LightningTower:
                        tower = new LightningTower(this, placeCoords[0], placeCoords[1]);
                        this.bits -= TowerCosts.getCostByType(type);
                        break;
                    case TowerType.BombTower:
                        tower = new BombTower(this, placeCoords[0], placeCoords[1]);
                        this.bits -= TowerCosts.getCostByType(type);
                        break;
                    case TowerType.FrozenTower:
                        tower = new FrozenTower(this, placeCoords[0], placeCoords[1]);
                        this.bits -= TowerCosts.getCostByType(type);
                        break;
                }
                this.grid[gridPos[1]][gridPos[0]] = tower;
                this.stats.addTowerPlaced(1);
            }
            else if (gridPos != null && this.grid[gridPos[1]][gridPos[0]] == 2 && type == TowerType.LinkTower) {
                //the link tower is handled extra because it can only be placed on the track
                var placeCoords = Constants.GridToXY(gridPos[0], gridPos[1]);
                var tower = void 0;
                tower = new LinkTower(this, placeCoords[0], placeCoords[1]);
                this.bits -= TowerCosts.getCostByType(type);
                this.grid[gridPos[1]][gridPos[0]] = tower;
                this.stats.addTowerPlaced(1);
            }
        }
    };
    //removes the tower from the grid and the array
    Game.prototype.removeTower = function (tower) {
        var gridPos = Constants.XYToGrid(tower.getPosX(), tower.getPosY());
        if (tower instanceof LinkTower) {
            this.grid[gridPos[1]][gridPos[0]] = 2;
        }
        else {
            this.grid[gridPos[1]][gridPos[0]] = 0;
        }
        this.towers = this.towers.filter(function (t) { return t !== tower; });
    };
    //bits are capped at 999999
    Game.prototype.addBits = function (amount) {
        this.bits += amount;
        this.stats.addBitsEarned(amount);
        if (this.bits > 999999) {
            this.bits = 999999;
        }
    };
    Game.prototype.pausePlay = function () {
        if (this.running) {
            this.running = false;
            this.stopLoop();
            this.drawer.draw();
        }
        else {
            this.running = true;
            this.startLoop();
        }
    };
    Game.prototype.end = function () {
        var element = document.getElementById("newGame");
        var canvas = document.getElementById("game-area");
        var rect = canvas.getBoundingClientRect();
        this.drawer.draw();
        this.drawer.drawGameOver();
        this.running = false;
        element.classList.remove("hidden");
        element.classList.add("active");
        document.getElementById("newGame").style.top = "" + (rect.top + 315).toString() + "px";
        element.style.left = "" + (rect.left + 288).toString() + "px";
    };
    Game.prototype.updatePositionNewGameButton = function () {
        var element = document.getElementById("newGame");
        var canvas = document.getElementById("game-area");
        var rect = canvas.getBoundingClientRect();
        if (!this.running) {
            element.classList.remove("hidden");
            element.classList.add("active");
            document.getElementById("newGame").style.top = "" + (rect.top + 315).toString() + "px";
            element.style.left = "" + (rect.left + 288).toString() + "px";
        }
    };
    Game.prototype.getCanvas = function () {
        return this.canvas;
    };
    Game.prototype.getContext = function () {
        return this.context;
    };
    Game.prototype.getTowers = function () {
        return this.towers;
    };
    Game.prototype.getShots = function () {
        return this.shots;
    };
    Game.prototype.getUnits = function () {
        return this.units;
    };
    Game.prototype.getProcessor = function () {
        return this.processor;
    };
    Game.prototype.getBackgroundImage = function () {
        return this.backgroundImage;
    };
    Game.prototype.getBits = function () {
        return this.bits;
    };
    Game.prototype.getWave = function () {
        return this.wave;
    };
    Game.prototype.getDragNDrop = function () {
        return this.mouseControl;
    };
    Game.prototype.getStart = function () {
        return [this.startX, this.startY];
    };
    Game.prototype.getRunning = function () {
        return this.running;
    };
    return Game;
}());
/// <reference path="../references.ts" />
var Processor = /** @class */ (function () {
    function Processor(maxhealth) {
        this.maxHealth = maxhealth;
        this.health = maxhealth;
        this.image = Pictures.processor;
        this.imageDestroyed = Pictures.processorDestroyed;
    }
    Processor.prototype.doDamage = function (amount) {
        this.health -= amount;
    };
    Processor.prototype.getHealthPercentage = function () {
        return (this.health / this.maxHealth);
    };
    Processor.prototype.getImages = function () {
        return [this.image, this.imageDestroyed, (this.health / this.maxHealth)];
    };
    return Processor;
}());
/// <reference path="../references.ts" />
var Stats = /** @class */ (function () {
    function Stats() {
        this.waveReached = 0;
        this.towersPlaced = 0;
        this.unitsKilled = 0;
        this.bitsEarned = 0;
    }
    Stats.prototype.addWaveReached = function (amount) {
        this.waveReached += amount;
    };
    Stats.prototype.addTowerPlaced = function (amount) {
        this.towersPlaced += amount;
    };
    Stats.prototype.addUnitKilled = function (amount) {
        this.unitsKilled += amount;
    };
    Stats.prototype.addBitsEarned = function (amount) {
        this.bitsEarned += amount;
    };
    Stats.prototype.getStats = function () {
        return [this.waveReached, this.towersPlaced, this.unitsKilled, this.bitsEarned];
    };
    return Stats;
}());
/// <reference path="../references.ts" />
var Balance = /** @class */ (function () {
    function Balance() {
    }
    //this class is used to maintain all the values to balance the game
    //so it is easier to keep an overview af the values and to find them
    /*RT = ReloadTime
    * SR = SearchRadius
    * BS = BulletSpeed
    * BD = BulletDamage
    * FrozenSF = Frozen Surface
    */
    Balance.HomingRT = 60;
    Balance.HomingSR = 100;
    Balance.HomingBS = 5;
    Balance.HomingBD = 7;
    Balance.SniperRT = 120;
    Balance.SniperSR = 150;
    Balance.SniperBS = 200;
    Balance.SniperBD = 50;
    Balance.SniperDamageFactor = 0.7;
    Balance.MGRT = 20;
    Balance.MGSR = 130;
    Balance.MGBS = 40;
    Balance.MGBD = 5;
    Balance.FrozenPeriod = 300;
    Balance.FrozenRT = Balance.FrozenPeriod + 300;
    Balance.FrozenRadius = 45;
    Balance.BombRT = 90;
    Balance.BombSR = 100;
    Balance.BombBS = 8;
    Balance.BombBD = 9;
    Balance.BombBurnDamage = 0.3;
    Balance.BombBurnTime = 50;
    Balance.BombBurnRadius = 30;
    Balance.LightningRT = 160;
    Balance.LightningSR = 70;
    Balance.LightningDamage = 20;
    Balance.LightningMaxBounces = 10;
    Balance.LightningBounceTime = 5;
    Balance.FrozenSFFreezeFactor = 0;
    Balance.FrozenSFFreezeTime = 180;
    Balance.FrozenSFCoolFactor = 0.5;
    Balance.FrozenSFCoolTime = 180;
    Balance.LinkHealth = 10;
    return Balance;
}());
/// <reference path="references.ts" />
window.onload = function () {
    //sections of the track in grids [x,y]
    var track = [[0, 5], [-9, 0], [0, 5], [7, 0], [0, 4], [5, 0], [0, -11], [5, 0], [0, 10]];
    /* 0 = empty
     * 1 = blocked
     * 2 = track
     * 3 = cpu*/
    var grid = [[0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 2, 1, 0, 2, 2, 2, 2, 2, 2, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 1, 2, 0, 0, 0, 0, 2, 0],
        [0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 0, 2, 0, 0, 1, 0, 2, 0],
        [0, 0, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 2, 0, 0, 0, 0, 2, 0],
        [0, 1, 2, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0, 2, 0, 0, 1, 0, 2, 0],
        [0, 1, 2, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 2, 0, 0, 1, 0, 2, 0],
        [0, 1, 2, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 2, 0, 0, 1, 0, 2, 0],
        [0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 1, 0, 2, 0, 1, 1, 0, 2, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 2, 0, 1, 1, 0, 2, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 2, 0, 1, 1, 0, 2, 0],
        [0, 0, 0, 0, 1, 1, 1, 1, 0, 2, 1, 1, 1, 1, 2, 0, 1, 1, 3, 3, 3],
        [0, 1, 0, 1, 1, 1, 1, 1, 0, 2, 2, 2, 2, 2, 2, 0, 1, 1, 3, 3, 3],
        [0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 3, 3, 3]];
    Pictures.init();
    var game = new Game(Pictures.world1, 11.5, 0.5, track, grid, 100);
    game.startLoop();
};
/// <reference path="../references.ts" />
var Tower = /** @class */ (function () {
    function Tower(game, posX, posY, reloadTime, searchRadius, image) {
        this.posX = posX;
        this.posY = posY;
        this.reloadTime = reloadTime;
        this.coolDown = 0;
        this.searchRadius = searchRadius;
        this.game = game;
        this.image = image;
    }
    //called every tick, used to update cooldown, call shoot
    //and reset cooldown if shoot was successful
    Tower.prototype.tick = function () {
        if (this.coolDown > 0) {
            this.coolDown--;
        }
        else {
            var success = this.shoot();
            if (success) {
                this.resetCooldown();
            }
        }
    };
    Tower.prototype.resetCooldown = function () {
        this.coolDown = this.reloadTime;
    };
    Tower.prototype.getImage = function () {
        return this.image;
    };
    Tower.prototype.getPosX = function () {
        return this.posX;
    };
    Tower.prototype.getPosY = function () {
        return this.posY;
    };
    return Tower;
}());
/// <reference path="../references.ts" />
var BombTower = /** @class */ (function (_super) {
    __extends(BombTower, _super);
    function BombTower(game, posX, posY) {
        var _this = _super.call(this, game, posX, posY, Balance.BombRT, Balance.BombSR, Pictures.bombTower) || this;
        _this.bulletSpeed = Balance.BombBS;
        _this.explosionDamage = Balance.BombBD;
        _this.burnDamage = Balance.BombBurnDamage;
        _this.burnTime = Balance.BombBurnTime;
        game.addTower(_this);
        return _this;
    }
    //shoots if unit in range
    BombTower.prototype.shoot = function () {
        var target = this.game.getFirstUnitInRadius(this.searchRadius, this.posX, this.posY);
        if (target != null) {
            new ExplosiveShot(this.game, target.getPosX(), target.getPosY(), this.posX, this.posY, Balance.BombBurnRadius, this.bulletSpeed, this.explosionDamage, this.burnDamage, this.burnTime);
            return true;
        }
        else {
            return false;
        }
    };
    return BombTower;
}(Tower));
/// <reference path="../references.ts" />
var FrozenTower = /** @class */ (function (_super) {
    __extends(FrozenTower, _super);
    function FrozenTower(game, posX, posY) {
        var _this = _super.call(this, game, posX, posY, Balance.FrozenRT, 0, Pictures.frozenTower) || this;
        _this.frozenPeriod = Balance.FrozenPeriod;
        _this.radius = Balance.FrozenRadius;
        game.addTower(_this);
        return _this;
    }
    //doesn't matter wether units are in range or not
    FrozenTower.prototype.shoot = function () {
        new FrozenSurface(this.game, this.posX, this.posY, this.frozenPeriod, this.radius);
        return true;
    };
    return FrozenTower;
}(Tower));
/// <reference path="../references.ts" />
var HomingTower = /** @class */ (function (_super) {
    __extends(HomingTower, _super);
    function HomingTower(game, posX, posY) {
        var _this = _super.call(this, game, posX, posY, Balance.HomingRT, Balance.HomingSR, Pictures.homingTower) || this;
        _this.bulletSpeed = Balance.HomingBS;
        _this.bulletDamage = Balance.HomingBD;
        game.addTower(_this);
        return _this;
    }
    //shoots if unit in range
    HomingTower.prototype.shoot = function () {
        var target = this.game.getFirstUnitInRadius(this.searchRadius, this.posX, this.posY);
        if (target != null) {
            new DirectedBall(this.game, target, this.bulletDamage, this.posX, this.posY, this.bulletSpeed);
            return true;
        }
        else {
            return false;
        }
    };
    return HomingTower;
}(Tower));
/// <reference path="../references.ts" />
var LightningTower = /** @class */ (function (_super) {
    __extends(LightningTower, _super);
    function LightningTower(game, posX, posY) {
        var _this = _super.call(this, game, posX, posY, Balance.LightningRT, Balance.LightningSR, Pictures.lightningTower) || this;
        _this.lightningColor = '#EE82EE';
        _this.lightningDamage = Balance.LightningDamage;
        _this.lightningMaxBounces = Balance.LightningMaxBounces;
        _this.lightningBounceTime = Balance.LightningBounceTime;
        game.addTower(_this);
        return _this;
    }
    //shoots if unit in range
    LightningTower.prototype.shoot = function () {
        var target = this.game.getFirstUnitInRadius(this.searchRadius, this.posX, this.posY);
        if (target != null) {
            new Lightning(this.game, target, this.lightningDamage, this.posX, this.posY, this.lightningMaxBounces, this.searchRadius, this.lightningBounceTime, this.lightningColor);
            return true;
        }
        else {
            return false;
        }
    };
    return LightningTower;
}(Tower));
/// <reference path="../references.ts" />
var LinkTower = /** @class */ (function (_super) {
    __extends(LinkTower, _super);
    function LinkTower(game, posX, posY) {
        var _this = _super.call(this, game, posX, posY, 0, 0, null) || this;
        _this.game.addTower(_this);
        //called in constructor because it is only done once at construction
        _this.shoot();
        return _this;
    }
    LinkTower.prototype.shoot = function () {
        new Connection(this.game, this.posX, this.posY, Balance.LinkHealth);
        return true;
    };
    //Override to prevent multiple Connection spawns
    LinkTower.prototype.tick = function () { };
    return LinkTower;
}(Tower));
/// <reference path="../references.ts" />
var MGTower = /** @class */ (function (_super) {
    __extends(MGTower, _super);
    function MGTower(game, posX, posY) {
        var _this = _super.call(this, game, posX, posY, Balance.MGRT, Balance.MGSR, Pictures.mGTower) || this;
        _this.bulletSpeed = Balance.MGBS;
        _this.bulletDamage = Balance.MGBD;
        game.addTower(_this);
        return _this;
    }
    //shoots if unit in range
    MGTower.prototype.shoot = function () {
        var target = this.game.getFirstUnitInRadius(this.searchRadius, this.posX, this.posY);
        if (target != null) {
            new StraightBall(this.game, target.getPosX(), target.getPosY(), this.bulletDamage, this.posX, this.posY, this.bulletSpeed);
            return true;
        }
        else {
            return false;
        }
    };
    return MGTower;
}(Tower));
/// <reference path="../references.ts" />
var SniperTower = /** @class */ (function (_super) {
    __extends(SniperTower, _super);
    function SniperTower(game, posX, posY) {
        var _this = _super.call(this, game, posX, posY, Balance.SniperRT, Balance.SniperSR, Pictures.sniperTower) || this;
        _this.bulletSpeed = Balance.SniperBS;
        _this.bulletDamage = Balance.SniperBD;
        _this.damageFactor = Balance.SniperDamageFactor;
        game.addTower(_this);
        return _this;
    }
    //shoots if unit in range
    SniperTower.prototype.shoot = function () {
        var target = this.game.getFirstUnitInRadius(this.searchRadius, this.posX, this.posY);
        if (target != null) {
            new SniperShot(this.game, target.getPosX(), target.getPosY(), this.bulletDamage, this.damageFactor, this.posX, this.posY, this.bulletSpeed);
            return true;
        }
        else {
            return false;
        }
    };
    return SniperTower;
}(Tower));
/// <reference path="../references.ts" />
var TowerCosts = /** @class */ (function () {
    function TowerCosts() {
    }
    TowerCosts.getCostByType = function (type) {
        switch (type) {
            case TowerType.SniperTower:
                return 300;
            case TowerType.MGTower:
                return 200;
            case TowerType.HomingTower:
                return 450;
            case TowerType.LightningTower:
                return 750;
            case TowerType.BombTower:
                return 950;
            case TowerType.FrozenTower:
                return 600;
            case TowerType.LinkTower:
                return 400;
        }
    };
    return TowerCosts;
}());
/// <reference path="../references.ts" />
var TowerType;
(function (TowerType) {
    TowerType[TowerType["SniperTower"] = 0] = "SniperTower";
    TowerType[TowerType["MGTower"] = 1] = "MGTower";
    TowerType[TowerType["HomingTower"] = 2] = "HomingTower";
    TowerType[TowerType["LightningTower"] = 3] = "LightningTower";
    TowerType[TowerType["BombTower"] = 4] = "BombTower";
    TowerType[TowerType["FrozenTower"] = 5] = "FrozenTower";
    TowerType[TowerType["LinkTower"] = 6] = "LinkTower";
})(TowerType || (TowerType = {}));
/// <reference path="../references.ts" />
var Shot = /** @class */ (function () {
    function Shot(game, damage, posX, posY) {
        this.damage = damage;
        this.posX = posX;
        this.posY = posY;
        this.game = game;
    }
    Shot.prototype.getPosX = function () {
        return this.posX;
    };
    Shot.prototype.getPosY = function () {
        return this.posY;
    };
    return Shot;
}());
/// <reference path="../references.ts" />
var SingleTargetShot = /** @class */ (function (_super) {
    __extends(SingleTargetShot, _super);
    function SingleTargetShot() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    SingleTargetShot.prototype.hit = function (unit) {
        unit.doDamage(this.damage);
        this.game.removeShot(this);
    };
    return SingleTargetShot;
}(Shot));
/// <reference path="../references.ts" />
var Connection = /** @class */ (function (_super) {
    __extends(Connection, _super);
    function Connection(game, posX, posY, maxHitUnits) {
        var _this = _super.call(this, game, 0, posX, posY) || this;
        _this.maxHitUnits = maxHitUnits;
        _this.hitUnits = 0;
        game.addShot(_this);
        return _this;
    }
    //nothing has to be done in tick for connection
    //tick() is neccessary to iterate over all Shots 
    Connection.prototype.tick = function () { };
    Connection.prototype.hit = function (unit) {
        unit.doDamage(unit.getMaxHealth());
        this.hitUnits++;
        //if the hit limit is exceeded remove the connection and the corresponding tower
        if (this.hitUnits >= this.maxHitUnits) {
            var tower = this.game.getTowerAt(this.posX, this.posY);
            if (tower != null) {
                this.game.removeTower(tower);
            }
            this.game.removeShot(this);
        }
    };
    Connection.prototype.getImage = function () {
        return [Pictures.connection, 1 - 0.7 * (this.hitUnits / this.maxHitUnits), 0];
    };
    return Connection;
}(Shot));
/// <reference path="../references.ts" />
var DirectedBall = /** @class */ (function (_super) {
    __extends(DirectedBall, _super);
    function DirectedBall(game, target, damage, posX, posY, bulletSpeed) {
        var _this = _super.call(this, game, damage, posX, posY) || this;
        _this.target = target;
        _this.bulletSpeed = bulletSpeed;
        game.addShot(_this);
        return _this;
    }
    DirectedBall.prototype.tick = function () {
        this.move();
    };
    DirectedBall.prototype.move = function () {
        //calculates the distance that has to be moved in x and y
        var x = this.target.getPosX() - this.posX;
        var y = this.target.getPosY() - this.posY;
        var normalizationFactor = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
        var moveX = x * this.bulletSpeed / normalizationFactor;
        var moveY = y * this.bulletSpeed / normalizationFactor;
        //calculates angle of bullet
        //atan2 has no definitiongaps
        this.angle = Math.atan2(moveY, moveX);
        //shot would move over unit, so it is moved on th position of the unit
        if (Math.abs(moveX) > Math.abs(x) || Math.abs(moveY) > Math.abs(y)) {
            this.posX = this.target.getPosX();
            this.posY = this.target.getPosY();
        }
        else {
            //normal moving
            this.posX += moveX;
            this.posY += moveY;
        }
    };
    DirectedBall.prototype.getImage = function () {
        return [Pictures.directedShot, 1, this.angle];
    };
    return DirectedBall;
}(SingleTargetShot));
/// <reference path="../references.ts" />
var ExplosiveShot = /** @class */ (function (_super) {
    __extends(ExplosiveShot, _super);
    function ExplosiveShot(game, targetX, targetY, posX, posY, radius, bulletSpeed, explosionDamage, burnDamage, burnTime) {
        var _this = _super.call(this, game, explosionDamage, posX, posY) || this;
        //convert the startpos and targetpos to a normalized vector (moveX, moveY) of the direction
        //moving with this vector is easier than the calculation with startpos and targetpos
        _this.distanceX = targetX - posX;
        _this.distanceY = targetY - posY;
        var normalizationFactor = Math.sqrt(Math.pow(_this.distanceX, 2) + Math.pow(_this.distanceY, 2));
        _this.moveX = _this.distanceX * bulletSpeed / normalizationFactor;
        _this.moveY = _this.distanceY * bulletSpeed / normalizationFactor;
        //calculates angle of bullet
        //atan2 has no definitiongaps
        _this.angle = Math.atan2(_this.moveY, _this.moveX);
        _this.radius = radius;
        _this.bulletSpeed = bulletSpeed;
        _this.burnDamage = burnDamage;
        _this.exploded = false;
        _this.maxBurnTime = burnTime;
        _this.burnedTime = 0;
        game.addShot(_this);
        return _this;
    }
    ExplosiveShot.prototype.tick = function () {
        if (!this.exploded) {
            this.move();
        }
        else {
            this.burn();
        }
    };
    //do the explosion (damage to all units in range)
    ExplosiveShot.prototype.hit = function () {
        var _this = this;
        if (!this.exploded) {
            this.exploded = true;
            var targets = this.game.getAllUnitsInRadiusSorted(this.radius, this.posX, this.posY);
            targets.forEach(function (unit) {
                unit.doDamage(_this.damage);
            });
        }
    };
    ExplosiveShot.prototype.move = function () {
        //would move over target, so move only to target
        if (Math.abs(this.distanceX) < Math.abs(this.moveX)) {
            this.posX += this.distanceX;
            this.posY += this.distanceY;
            this.distanceX = 0;
            this.distanceY = 0;
            this.hit();
        }
        else {
            this.posX += this.moveX;
            this.posY += this.moveY;
            this.distanceX -= this.moveX;
            this.distanceY -= this.moveY;
        }
    };
    //does burn damage to all units in range
    //removes shot if burning ends
    ExplosiveShot.prototype.burn = function () {
        var _this = this;
        if (this.burnedTime < this.maxBurnTime) {
            this.burnedTime++;
            var targets = this.game.getAllUnitsInRadiusSorted(this.radius, this.posX, this.posY);
            targets.forEach(function (unit) {
                unit.doDamage(_this.burnDamage);
            });
        }
        else {
            this.game.removeShot(this);
        }
    };
    //burning is animated, therefore multiple pictures
    ExplosiveShot.prototype.getImage = function () {
        if (this.exploded) {
            var factor = void 0;
            factor = this.burnedTime / this.maxBurnTime;
            if (factor > 0.9) {
                return [Pictures.explosiveBurning1, 0.5, 0];
            }
            else if (factor > 0.8) {
                return [Pictures.explosiveBurning2, 0.5, 0];
            }
            else if (factor > 0.7) {
                return [Pictures.explosiveBurning3, 0.5, 0];
            }
            else if (factor > 0.6) {
                return [Pictures.explosiveBurning4, 0.5, 0];
            }
            else if (factor > 0.5) {
                return [Pictures.explosiveBurning5, 0.5, 0];
            }
            else if (factor > 0.4) {
                return [Pictures.explosiveBurning6, 0.5, 0];
            }
            else if (factor > 0.3) {
                return [Pictures.explosiveBurning7, 0.5, 0];
            }
            else if (factor > 0.2) {
                return [Pictures.explosiveBurning8, 0.5, 0];
            }
            else if (factor > 0.1) {
                return [Pictures.explosiveBurning9, 0.5, 0];
            }
            else {
                return [Pictures.explosiveBurning10, 0.5, 0];
            }
        }
        else {
            return [Pictures.explosiveShot, 1, this.angle];
        }
    };
    return ExplosiveShot;
}(Shot));
/// <reference path="../references.ts" />
var FrozenSurface = /** @class */ (function (_super) {
    __extends(FrozenSurface, _super);
    function FrozenSurface(game, posX, posY, frozenPeriod, radius) {
        var _this = _super.call(this, game, 0, posX, posY) || this;
        _this.frozenPeriod = frozenPeriod;
        _this.maxFrozenTime = frozenPeriod;
        _this.radius = radius;
        _this.freeze();
        game.addShot(_this);
        return _this;
    }
    FrozenSurface.prototype.tick = function () {
        if (this.frozenPeriod > 0) {
            this.frozenPeriod--;
            this.slow();
        }
        else {
            this.game.removeShot(this);
        }
    };
    //adds a slow effect to all units in range
    FrozenSurface.prototype.slow = function () {
        var units = this.game.getAllUnitsInRadiusSorted(this.radius, this.posX, this.posY);
        var slowEffect = new SlowEffect(this, Balance.FrozenSFCoolFactor, Balance.FrozenSFCoolTime);
        units.forEach(function (unit) {
            unit.addSlowEffect(slowEffect);
        });
    };
    //freezes all unit in range
    FrozenSurface.prototype.freeze = function () {
        var _this = this;
        var units;
        units = this.game.getAllUnitsInRadiusSorted(this.radius, this.posX, this.posY);
        units.forEach(function (unit) {
            var slowEffect = new SlowEffect(_this, Balance.FrozenSFFreezeFactor, Balance.FrozenSFFreezeTime);
            unit.addSlowEffect(slowEffect);
        });
    };
    //hit is not used for FrozenSurface, slow will be done in tick()
    //hit function is neccesary because itaration through all shot isn't possible without
    FrozenSurface.prototype.hit = function () { };
    FrozenSurface.prototype.getImage = function () {
        var opacity = 0.2 + (this.frozenPeriod / this.maxFrozenTime) * 0.8;
        return [Pictures.frozenSurface, opacity, 0];
    };
    return FrozenSurface;
}(Shot));
/// <reference path="../references.ts" />
var Lightning = /** @class */ (function (_super) {
    __extends(Lightning, _super);
    function Lightning(game, target, damage, posX, posY, maxHits, bounceRadius, bounceTime, color) {
        var _this = _super.call(this, game, damage, posX, posY) || this;
        _this.wayOfLightning = new Array();
        _this.wayOfLightning.push(target);
        _this.maxHits = maxHits;
        _this.bounceRadius = bounceRadius;
        _this.bounceTime = bounceTime;
        _this.bounceCooldown = bounceTime;
        _this.color = color;
        _this.game.addShot(_this);
        target.doDamage(damage);
        return _this;
    }
    Lightning.prototype.tick = function () {
        if (this.bounceCooldown > 0) {
            this.bounceCooldown--;
            return;
        }
        else {
            //lightning can bounce (time)
            this.bounceCooldown = this.bounceTime;
            if (this.wayOfLightning.length < this.maxHits) {
                //lightning can bounce (bounce limit)
                var lastUnit = this.wayOfLightning[this.wayOfLightning.length - 1];
                var tipX = lastUnit.getPosX();
                var tipY = lastUnit.getPosY();
                var possibleTargets = this.game.getAllUnitsInRadiusSorted(this.bounceRadius, tipX, tipY);
                //scans possibleTargets for units not yet affected by this ligthning
                if (possibleTargets.length > 0) {
                    var hit = false;
                    var _loop_1 = function (i) {
                        if (this_1.wayOfLightning.filter(function (u) { return u == possibleTargets[i]; }).length == 0) {
                            this_1.wayOfLightning.push(possibleTargets[i]);
                            possibleTargets[i].doDamage(this_1.damage);
                            hit = true;
                            return { value: void 0 };
                        }
                    };
                    var this_1 = this;
                    for (var i = 0; i < possibleTargets.length; i++) {
                        var state_1 = _loop_1(i);
                        if (typeof state_1 === "object")
                            return state_1.value;
                    }
                    //could not hit a new unit (all units in range are already hit)
                    if (!hit) {
                        this.game.removeShot(this);
                    }
                }
                else {
                    //could not hit a new unit (no unit in range)
                    this.game.removeShot(this);
                }
                //already max amount of units hit
            }
            else {
                this.game.removeShot(this);
            }
        }
    };
    //hit is not used for ligthning, damage will be done in move
    //hit function is neccesary because itaration through all shot isn't possible without
    Lightning.prototype.hit = function (unit) { };
    Lightning.prototype.getImage = function () {
        return null;
    };
    Lightning.prototype.getColor = function () {
        return this.color;
    };
    Lightning.prototype.getWayOfLightning = function () {
        return this.wayOfLightning;
    };
    return Lightning;
}(Shot));
/// <reference path="../references.ts" />
var StraightBall = /** @class */ (function (_super) {
    __extends(StraightBall, _super);
    function StraightBall(game, targetX, targetY, damage, posX, posY, bulletSpeed, superCall) {
        if (superCall === void 0) { superCall = false; }
        var _this = _super.call(this, game, damage, posX, posY) || this;
        //convert the startpos and targetpos to a normalized vector (moveX, moveY) of the direction
        //moving with this vector is easier than the calculation with startpos and targetpos
        _this.distanceX = targetX - posX;
        _this.distanceY = targetY - posY;
        var normalizationFactor = Math.sqrt(Math.pow(_this.distanceX, 2) + Math.pow(_this.distanceY, 2));
        _this.moveX = _this.distanceX * bulletSpeed / normalizationFactor;
        _this.moveY = _this.distanceY * bulletSpeed / normalizationFactor;
        //calculates angle of bullet
        //atan2 has no definitiongaps
        _this.angle = Math.atan2(_this.moveY, _this.moveX);
        _this.bulletSpeed = bulletSpeed;
        //neccessary because snipershot extends this and the shot shouldn't be added two times
        if (!superCall) {
            game.addShot(_this);
        }
        return _this;
    }
    StraightBall.prototype.tick = function () {
        //remove if shot reached goal last tick
        if (this.distanceX == 0 && this.distanceY == 0) {
            this.game.removeShot(this);
        }
        else {
            this.move();
        }
    };
    StraightBall.prototype.move = function () {
        //would move over target, so move only to target
        if (Math.abs(this.distanceX) < Math.abs(this.moveX)) {
            this.posX += this.distanceX;
            this.posY += this.distanceY;
            this.distanceX = 0;
            this.distanceY = 0;
        }
        //move normal distance
        else {
            this.posX += this.moveX;
            this.posY += this.moveY;
            this.distanceX -= this.moveX;
            this.distanceY -= this.moveY;
        }
    };
    StraightBall.prototype.getImage = function () {
        return [Pictures.straightShot, 1, this.angle];
    };
    return StraightBall;
}(SingleTargetShot));
/// <reference path="../references.ts" />
var SniperShot = /** @class */ (function (_super) {
    __extends(SniperShot, _super);
    function SniperShot(game, targetX, targetY, damage, damageFactor, posX, posY, bulletSpeed) {
        var _this = _super.call(this, game, targetX, targetY, damage, posX, posY, bulletSpeed, true) || this;
        _this.damageFactor = damageFactor;
        game.addShot(_this);
        return _this;
    }
    SniperShot.prototype.hit = function (unit) {
        //units health is lower or equal to the bullet damage --> do this damage
        if (unit.getHealth() <= this.damage) {
            unit.doDamage(this.damage);
        }
        else {
            //unit has more health than the bullet damage --> do procentual damage to the units health
            unit.doDamage(unit.getHealth() * this.damageFactor);
        }
        this.game.removeShot(this);
    };
    SniperShot.prototype.getImage = function () {
        return [Pictures.sniperShot, 1, this.angle];
    };
    return SniperShot;
}(StraightBall));
/// <reference path="../references.ts" />
var Drawer = /** @class */ (function () {
    function Drawer(game) {
        this.game = game;
    }
    //draws a picture on the canvas 
    Drawer.prototype.drawPicture = function (picture, x, y, alpha, rotation) {
        if (alpha === void 0) { alpha = 1; }
        if (rotation === void 0) { rotation = 0; }
        var posX = 0;
        var posY = 0;
        //draw with rotation if set
        if (rotation != 0) {
            //rotation is done through a rotation around the picture middle, then the picture is drawn and afterwards the rotation is undone (restore)
            posX = -(picture.width / 2);
            posY = -(picture.height / 2);
            this.game.getContext().save();
            this.game.getContext().translate(x, y);
            this.game.getContext().rotate(rotation);
        }
        else {
            posX = x - (picture.width / 2);
            posY = y - (picture.height / 2);
        }
        this.game.getContext().globalAlpha = alpha;
        this.game.getContext().drawImage(picture, posX, posY);
        this.game.getContext().globalAlpha = 1;
        if (rotation != 0) {
            this.game.getContext().restore();
        }
    };
    Drawer.prototype.draw = function () {
        var _this = this;
        //clear canvas
        this.game.getContext().clearRect(0, 0, this.game.getCanvas().width, this.game.getCanvas().height);
        this.game.getContext().drawImage(this.game.getBackgroundImage(), 0, 0);
        this.game.getTowers().forEach(function (tower) {
            _this.drawTower(tower);
        });
        //explosive and forzen are drawn below (before) the units
        this.game.getShots().forEach(function (shot) {
            if ((shot instanceof ExplosiveShot) || (shot instanceof FrozenSurface)) {
                _this.drawShot(shot);
            }
        });
        this.game.getUnits().forEach(function (unit) {
            _this.drawUnit(unit);
        });
        this.drawProcessor();
        //draw the rest of the shots
        this.game.getShots().forEach(function (shot) {
            if (shot instanceof Lightning) {
                _this.drawLigthning(shot);
            }
            else if (!(shot instanceof ExplosiveShot) && !(shot instanceof FrozenSurface)) {
                _this.drawShot(shot);
            }
        });
        this.game.getContext().drawImage(Pictures.rightMenu, 21 * Constants.gridSize, 2 * Constants.gridSize);
        //this.game.getContext().drawImage(Pictures.browser, 9 * Constants.gridSize, 0);
        //draw bits
        this.draw7SegmentDisplay(18.5, 0.5, this.game.getBits());
        //draw number of wave
        this.draw7SegmentDisplay(0.5, 0.5, this.game.getWave().getNumberOfWave());
        this.drawCooldownUntilNextWave();
        this.drawPausePlay();
        this.drawDragNDrop();
    };
    Drawer.prototype.drawUnit = function (unit) {
        var _a;
        var image;
        var opacity;
        _a = unit.getImageWithOpacity(), image = _a[0], opacity = _a[1];
        this.drawPicture(image, unit.getPosX(), unit.getPosY(), opacity);
    };
    Drawer.prototype.drawShot = function (shot) {
        var _a;
        var image;
        var opacity;
        var angle;
        _a = shot.getImage(), image = _a[0], opacity = _a[1], angle = _a[2];
        this.drawPicture(image, shot.getPosX(), shot.getPosY(), opacity, angle);
    };
    Drawer.prototype.drawTower = function (tower) {
        var image = tower.getImage();
        if (image != null) {
            this.drawPicture(image, tower.getPosX(), tower.getPosY());
        }
    };
    //a lightning is drawn line by line from one unit to the next
    Drawer.prototype.drawLigthning = function (lightning) {
        var targets = lightning.getWayOfLightning();
        var color = lightning.getColor();
        this.drawLightningLine(lightning.getPosX(), lightning.getPosY(), targets[0].getPosX(), targets[0].getPosY(), color);
        for (var i = 1; i < targets.length; i++) {
            this.drawLightningLine(targets[i - 1].getPosX(), targets[i - 1].getPosY(), targets[i].getPosX(), targets[i].getPosY(), color);
        }
    };
    //a line of a lightning is drawn by two overlaying normal canvas lines
    Drawer.prototype.drawLightningLine = function (fromX, fromY, toX, toY, color) {
        this.game.getContext().beginPath();
        this.game.getContext().moveTo(fromX, fromY);
        this.game.getContext().lineTo(toX, toY);
        this.game.getContext().lineWidth = 3;
        this.game.getContext().strokeStyle = '#FFFFFF';
        this.game.getContext().stroke();
        this.game.getContext().beginPath();
        this.game.getContext().moveTo(fromX, fromY);
        this.game.getContext().lineTo(toX, toY);
        this.game.getContext().lineWidth = 2;
        this.game.getContext().strokeStyle = color;
        this.game.getContext().stroke();
    };
    Drawer.prototype.drawProcessor = function () {
        var _a;
        var image;
        var imageDestroyed;
        var opacity;
        _a = this.game.getProcessor().getImages(), image = _a[0], imageDestroyed = _a[1], opacity = _a[2];
        this.drawPicture(image, Constants.prozessorPos[0], Constants.prozessorPos[1]);
        this.drawPicture(imageDestroyed, Constants.prozessorPos[0], Constants.prozessorPos[1], 1 - opacity);
    };
    Drawer.prototype.draw7SegmentDisplay = function (startX, startY, displayValue) {
        var digitString = (displayValue + '').split('');
        var digits = digitString.map(Number);
        var drawingPosX = startX;
        //0.(0-5)
        //draw digits not set by stored value to 0
        for (var i = 1; i <= (6 - digits.length); i++) {
            this.drawPicture(Pictures.SSeg0, drawingPosX * Constants.gridSize, startY * Constants.gridSize);
            drawingPosX++;
        }
        for (var _i = 0, digits_1 = digits; _i < digits_1.length; _i++) {
            var i = digits_1[_i];
            this.drawPicture(Pictures.getSSegByDigit(i), drawingPosX * Constants.gridSize, startY * Constants.gridSize);
            drawingPosX++;
        }
    };
    Drawer.prototype.drawCooldownUntilNextWave = function () {
        var progressOfCooldown = this.game.getWave().getProgressOfCooldown();
        var drawingPosX = 0.5;
        var drawingPosY = 1.5;
        for (var i = 1; i <= progressOfCooldown; i++) {
            this.drawPicture(Pictures.ledOn, drawingPosX * Constants.gridSize, drawingPosY * Constants.gridSize);
            drawingPosX++;
        }
        for (var i = 1; i <= (6 - progressOfCooldown); i++) {
            this.drawPicture(Pictures.ledOff, drawingPosX * Constants.gridSize, drawingPosY * Constants.gridSize);
            drawingPosX++;
        }
    };
    Drawer.prototype.drawDragNDrop = function () {
        var image = this.game.getDragNDrop().getDrawImage();
        if (image != null) {
            var _a = this.game.getDragNDrop().getDrawCords(), x = _a[0], y = _a[1];
            this.drawPicture(image, x, y);
        }
    };
    Drawer.prototype.drawPausePlay = function () {
        if (this.game.getRunning()) {
            this.drawPicture(Pictures.pause, 15.5 * Constants.gridSize, 1 * Constants.gridSize);
        }
        else {
            this.drawPicture(Pictures.play, 15.5 * Constants.gridSize, 1 * Constants.gridSize);
        }
    };
    //numbers over 999999999 are converted to exponential
    Drawer.prototype.drawGameOver = function () {
        var _a;
        this.game.getContext().drawImage(Pictures.gameover, 0, 0);
        this.game.getContext().font = "15px Courier New";
        var text;
        var waveReached;
        var towersPlaced;
        var unitsKilled;
        var bitsEarned;
        _a = this.game.stats.getStats(), waveReached = _a[0], towersPlaced = _a[1], unitsKilled = _a[2], bitsEarned = _a[3];
        text = "Erreichte Welle:   ";
        if (waveReached > 999999999) {
            text += waveReached.toExponential(2);
        }
        else {
            text += waveReached;
        }
        this.game.getContext().fillText(text, 220, 220);
        text = "Vernichtete Viren: ";
        if (unitsKilled > 999999999) {
            text += unitsKilled.toExponential(2);
        }
        else {
            text += unitsKilled;
        }
        this.game.getContext().fillText(text, 220, 245);
        text = "Erhaltene Bits:    ";
        if (bitsEarned > 999999999) {
            text += bitsEarned.toExponential(2);
        }
        else {
            text += bitsEarned;
        }
        this.game.getContext().fillText(text, 220, 270);
        text = "Anzahl der Tower:  " + towersPlaced;
        this.game.getContext().fillText(text, 220, 295);
    };
    return Drawer;
}());
/// <reference path="../references.ts" />
var Pictures = /** @class */ (function () {
    function Pictures() {
    }
    Pictures.init = function () {
        this.sniperTower = new Image();
        this.sniperTower.src = "/images/towers/sniperTower_stufe1.png";
        this.sniperTowerDND = new Image();
        this.sniperTowerDND.src = "/images/towers_circle/sniperTower_circle.png";
        this.mGTower = new Image();
        this.mGTower.src = "/images/towers/mGTower_stufe1.png";
        this.mGTowerDND = new Image();
        this.mGTowerDND.src = "/images/towers_circle/mGTower_circle.png";
        this.homingTower = new Image();
        this.homingTower.src = "/images/towers/homingTower_stufe1.png";
        this.homingTowerDND = new Image();
        this.homingTowerDND.src = "/images/towers_circle/homingTower_circle.png";
        this.lightningTower = new Image();
        this.lightningTower.src = "/images/towers/lightningTower_stufe1.png";
        this.lightningTowerDND = new Image();
        this.lightningTowerDND.src = "/images/towers_circle/lightningTower_circle.png";
        this.bombTower = new Image();
        this.bombTower.src = "/images/towers/bombTower_stufe1.png";
        this.bombTowerDND = new Image();
        this.bombTowerDND.src = "/images/towers_circle/bombTower_circle.png";
        this.frozenTower = new Image();
        this.frozenTower.src = "/images/towers/frozenTower_stufe1.png";
        this.frozenTowerDND = new Image();
        this.frozenTowerDND.src = "/images/towers_circle/frozenTower_circle.png";
        this.straightShot = new Image();
        this.straightShot.src = "/images/mgShot.png";
        this.directedShot = new Image();
        this.directedShot.src = "/images/directedShot.png";
        this.explosiveShot = new Image();
        this.explosiveShot.src = "/images/bomb.png";
        this.sniperShot = new Image();
        this.sniperShot.src = "/images/sniperShot.png";
        this.explosiveBurning1 = new Image();
        this.explosiveBurning1.src = "/images/explosion1.png";
        this.explosiveBurning2 = new Image();
        this.explosiveBurning2.src = "/images/explosion2.png";
        this.explosiveBurning3 = new Image();
        this.explosiveBurning3.src = "/images/explosion3.png";
        this.explosiveBurning4 = new Image();
        this.explosiveBurning4.src = "/images/explosion4.png";
        this.explosiveBurning5 = new Image();
        this.explosiveBurning5.src = "/images/explosion5.png";
        this.explosiveBurning6 = new Image();
        this.explosiveBurning6.src = "/images/explosion6.png";
        this.explosiveBurning7 = new Image();
        this.explosiveBurning7.src = "/images/explosion7.png";
        this.explosiveBurning8 = new Image();
        this.explosiveBurning8.src = "/images/explosion8.png";
        this.explosiveBurning9 = new Image();
        this.explosiveBurning9.src = "/images/explosion9.png";
        this.explosiveBurning10 = new Image();
        this.explosiveBurning10.src = "/images/explosion10.png";
        this.frozenSurface = new Image();
        this.frozenSurface.src = "/images/frozenSurface.png";
        this.connection = new Image();
        this.connection.src = "/images/connection.png";
        this.unit = new Image();
        this.unit.src = "/images/unit.png";
        this.processor = new Image();
        this.processor.src = "/images/prozessor.png";
        this.processorDestroyed = new Image();
        this.processorDestroyed.src = "/images/prozessor_kaputt.png";
        this.rightMenu = new Image();
        this.rightMenu.src = "/images/right_menu.png";
        this.gameover = new Image();
        this.gameover.src = "/images/gameover.png";
        this.pause = new Image();
        this.pause.src = "/images/pause.png";
        this.play = new Image();
        this.play.src = "/images/play.png";
        this.SSeg0 = new Image();
        this.SSeg0.src = "/images/SSeg/SSeg0.png";
        this.SSeg1 = new Image();
        this.SSeg1.src = "/images/SSeg/SSeg1.png";
        this.SSeg2 = new Image();
        this.SSeg2.src = "/images/SSeg/SSeg2.png";
        this.SSeg3 = new Image();
        this.SSeg3.src = "/images/SSeg/SSeg3.png";
        this.SSeg4 = new Image();
        this.SSeg4.src = "/images/SSeg/SSeg4.png";
        this.SSeg5 = new Image();
        this.SSeg5.src = "/images/SSeg/SSeg5.png";
        this.SSeg6 = new Image();
        this.SSeg6.src = "/images/SSeg/SSeg6.png";
        this.SSeg7 = new Image();
        this.SSeg7.src = "/images/SSeg/SSeg7.png";
        this.SSeg8 = new Image();
        this.SSeg8.src = "/images/SSeg/SSeg8.png";
        this.SSeg9 = new Image();
        this.SSeg9.src = "/images/SSeg/SSeg9.png";
        this.ledOn = new Image();
        this.ledOn.src = "/images/LED_an.png";
        this.ledOff = new Image();
        this.ledOff.src = "/images/LED_aus.png";
        this.world1 = new Image();
        this.world1.src = "/images/world1.png";
    };
    Pictures.getSSegByDigit = function (digit) {
        switch (digit) {
            case 1:
                return this.SSeg1;
            case 2:
                return this.SSeg2;
            case 3:
                return this.SSeg3;
            case 4:
                return this.SSeg4;
            case 5:
                return this.SSeg5;
            case 6:
                return this.SSeg6;
            case 7:
                return this.SSeg7;
            case 8:
                return this.SSeg8;
            case 9:
                return this.SSeg9;
            default:
                return this.SSeg0;
        }
    };
    return Pictures;
}());
/// <reference path="../references.ts" />
var Constants = /** @class */ (function () {
    function Constants() {
    }
    //convert coordinates to a grid square
    Constants.XYToGrid = function (posX, posY) {
        var lokalX = (posX) / this.gridSize;
        var lokalY = (posY - 60) / this.gridSize;
        if (lokalX < 0 || lokalY < 0) {
            return null;
        }
        lokalX = Math.floor(lokalX);
        lokalY = Math.floor(lokalY);
        return [lokalX, lokalY];
    };
    //convert a grid square to coordinates (middle of square)
    Constants.GridToXY = function (gridX, gridY) {
        var lokalX = (gridX + 0.5) * this.gridSize;
        var lokalY = (gridY + 2.5) * this.gridSize;
        return [lokalX, lokalY];
    };
    //converts track from grid squares to coordinates (pixels)
    Constants.convertTrack = function (track) {
        track.forEach(function (e) {
            e[0] *= Constants.gridSize;
            e[1] *= Constants.gridSize;
        });
    };
    Constants.gridSize = 30;
    Constants.prozessorPos = [19.5 * Constants.gridSize, 14.5 * Constants.gridSize];
    Constants.startingBits = 350;
    Constants.unitDamage = 1;
    Constants.unitValue = 12;
    return Constants;
}());
/// <reference path="../references.ts" />
var MouseControl = /** @class */ (function () {
    function MouseControl(game, canvas) {
        this.game = game;
        this.canvas = canvas;
        this.canvas.onmousedown = this.onDown.bind(this);
        this.canvas.onmouseleave = this.onLeave.bind(this);
        this.canvas.onmousemove = this.onMove.bind(this);
        this.canvas.onmouseup = this.onUp.bind(this);
    }
    MouseControl.prototype.onDown = function (e) {
        this.drawCoords = [e.pageX - this.canvas.offsetLeft, e.pageY - this.canvas.offsetTop];
        //clicked pause/play?
        if ((this.drawCoords[0] > 15 * Constants.gridSize) && (this.drawCoords[0] <= 16 * Constants.gridSize) && (this.drawCoords[1] >= 0 * Constants.gridSize) && (this.drawCoords[1] <= 2 * Constants.gridSize)) {
            this.game.pausePlay();
        }
        else if (this.updatePickupType()) {
            //clicked on a towertype
            this.dragging = true;
        }
        else {
            //clicked on something else
            this.drawCoords = undefined;
            this.dragging = false;
        }
    };
    //tries to place tower if one was dragged
    MouseControl.prototype.onUp = function (e) {
        if (this.dragging) {
            this.dragging = false;
            this.game.placeTower(this.towerType, this.drawCoords[0], this.drawCoords[1]);
            this.drawCoords = null;
            this.image = null;
        }
    };
    //update mouse position
    MouseControl.prototype.onMove = function (e) {
        this.drawCoords = [e.pageX - this.canvas.offsetLeft, e.pageY - this.canvas.offsetTop];
    };
    MouseControl.prototype.onLeave = function (e) {
        this.dragging = false;
        this.drawCoords = null;
    };
    MouseControl.prototype.updatePickupType = function () {
        var towerType = null;
        var image = null;
        //check for x-range of the pickup area
        //check for the corresponding y-range for the towers
        //if one was clicked set type and picture
        if ((this.drawCoords[0] > 21 * Constants.gridSize) && (this.drawCoords[0] <= 24 * Constants.gridSize)) {
            if ((this.drawCoords[1] > 2 * Constants.gridSize) && (this.drawCoords[1] <= 4 * Constants.gridSize)) {
                towerType = TowerType.SniperTower;
                image = Pictures.sniperTowerDND;
            }
            else if ((this.drawCoords[1] > 4 * Constants.gridSize) && (this.drawCoords[1] <= 6 * Constants.gridSize)) {
                towerType = TowerType.MGTower;
                image = Pictures.mGTowerDND;
            }
            else if ((this.drawCoords[1] > 6 * Constants.gridSize) && (this.drawCoords[1] <= 8 * Constants.gridSize)) {
                towerType = TowerType.HomingTower;
                image = Pictures.homingTowerDND;
            }
            else if ((this.drawCoords[1] > 8 * Constants.gridSize) && (this.drawCoords[1] <= 10 * Constants.gridSize)) {
                towerType = TowerType.LightningTower;
                image = Pictures.lightningTowerDND;
            }
            else if ((this.drawCoords[1] > 10 * Constants.gridSize) && (this.drawCoords[1] <= 12 * Constants.gridSize)) {
                towerType = TowerType.BombTower;
                image = Pictures.bombTowerDND;
            }
            else if ((this.drawCoords[1] > 12 * Constants.gridSize) && (this.drawCoords[1] <= 14 * Constants.gridSize)) {
                towerType = TowerType.FrozenTower;
                image = Pictures.frozenTowerDND;
            }
            else if ((this.drawCoords[1] > 14 * Constants.gridSize) && (this.drawCoords[1] <= 16 * Constants.gridSize)) {
                towerType = TowerType.LinkTower;
                image = Pictures.connection;
            }
        }
        if (towerType != null && image != null) {
            this.towerType = towerType;
            this.image = image;
            return true;
        }
        else {
            this.towerType = null;
            this.image = null;
            return false;
        }
    };
    MouseControl.prototype.getDrawImage = function () {
        if (this.dragging) {
            return this.image;
        }
        else {
            return null;
        }
    };
    MouseControl.prototype.getDrawCords = function () {
        return this.drawCoords;
    };
    return MouseControl;
}());
/// <reference path="../references.ts" />
var SlowEffect = /** @class */ (function () {
    function SlowEffect(sourceTower, factor, duration) {
        this.sourceTower = sourceTower;
        this.factor = factor;
        this.duration = duration;
    }
    SlowEffect.prototype.updateDuration = function () {
        this.duration--;
    };
    SlowEffect.prototype.getSourceTower = function () {
        return this.sourceTower;
    };
    SlowEffect.prototype.getFactor = function () {
        return this.factor;
    };
    SlowEffect.prototype.getDuration = function () {
        return this.duration;
    };
    return SlowEffect;
}());
/// <reference path="../references.ts" />
var Unit = /** @class */ (function () {
    function Unit(game, health, speed) {
        var _a;
        this.health = health;
        this.maxHealth = health;
        this.speed = speed;
        _a = game.getStart(), this.posX = _a[0], this.posY = _a[1];
        this.progress = 0;
        this.game = game;
        this.slowEffects = new Array();
        game.addUnit(this);
    }
    Unit.prototype.tick = function () {
        this.updateSlowEffects();
        this.move();
    };
    Unit.prototype.move = function () {
        this.progress += this.speed * this.calculateSlowingFactor();
        var newPos = this.game.getPosFromProgress(this.progress);
        if (newPos != null) {
            this.posX = newPos[0], this.posY = newPos[1];
        }
        else {
            this.game.removeUnit(this);
        }
    };
    Unit.prototype.doDamage = function (amount) {
        this.health -= amount;
        if (this.health <= 0) {
            this.game.addBits(Constants.unitValue);
            this.game.stats.addUnitKilled(1);
            this.game.removeUnit(this);
        }
    };
    Unit.prototype.addSlowEffect = function (slowEffect) {
        var effectAdded = false;
        //updates an old effect from the same tower to the new one (when factor is (s)lower)
        for (var i = 0; i < this.slowEffects.length && !effectAdded; i++) {
            if (this.slowEffects[i].getSourceTower == slowEffect.getSourceTower) {
                if (this.slowEffects[i].getFactor() >= slowEffect.getFactor()) {
                    this.slowEffects[i] = slowEffect;
                    effectAdded = true;
                }
            }
        }
        if (!effectAdded) {
            this.slowEffects.push(slowEffect);
        }
    };
    //multiplies up all factor
    Unit.prototype.calculateSlowingFactor = function () {
        var slowingFactor = 1;
        this.slowEffects.forEach(function (slowEffect) {
            slowingFactor *= slowEffect.getFactor();
        });
        return slowingFactor;
    };
    //ticks all slow effects and removes the ones timed out
    Unit.prototype.updateSlowEffects = function () {
        this.slowEffects.forEach(function (slowEffect) {
            if (slowEffect.getDuration() > 0) {
                slowEffect.updateDuration();
            }
        });
        this.slowEffects = this.slowEffects.filter(function (SlowEffect) { return SlowEffect.getDuration() > 0; });
    };
    Unit.prototype.getMaxHealth = function () {
        return this.maxHealth;
    };
    Unit.prototype.getHealth = function () {
        return this.health;
    };
    Unit.prototype.getProgress = function () {
        return this.progress;
    };
    Unit.prototype.getPosX = function () {
        return this.posX;
    };
    Unit.prototype.getPosY = function () {
        return this.posY;
    };
    Unit.prototype.getImageWithOpacity = function () {
        var opacity = 0.5 + ((this.health / this.maxHealth) / 2);
        return [Pictures.unit, opacity];
    };
    return Unit;
}());
/// <reference path="../references.ts" />
var Wave = /** @class */ (function () {
    function Wave(game) {
        this.reset();
        this.numberOfWave = 0;
        this.unitBuildStatus = 0;
        this.game = game;
        this.healthFunctionSwitch = 30;
    }
    Wave.prototype.tick = function () {
        //normal tick (time between waves)
        if (this.coolDown > 1) {
            this.coolDown--;
        }
        else if (this.coolDown == 1) {
            //start new wave
            this.numberOfWave++;
            this.game.stats.addWaveReached(1);
            this.coolDown--;
            this.calculate();
        }
        else {
            //wave is finished
            if (this.waveDuration == 0) {
                this.reset();
                this.calculate();
            }
            else {
                //wave is in progress
                this.unitBuildStatus += this.averageNumberOfUnits;
                if (this.unitBuildStatus > 1 && this.waveDuration > 1) {
                    this.createUnits(Math.floor(this.unitBuildStatus));
                    this.unitBuildStatus -= Math.floor(this.unitBuildStatus);
                    this.waveDuration--;
                }
                else if (this.waveDuration == 1) {
                    this.createUnits(Math.floor(this.unitBuildStatus));
                    this.unitBuildStatus = 0;
                    this.waveDuration = 0;
                }
                else {
                    this.waveDuration--;
                }
            }
        }
    };
    Wave.prototype.reset = function () {
        this.coolDown = 600;
        this.waveDuration = 600;
    };
    Wave.prototype.createUnits = function (numberOfUnits) {
        while (numberOfUnits >= 1) {
            var newUnit = new Unit(this.game, this.healthOfUnits, (1 + ((Math.random() / 4) - 0.125)) * this.speedOfUnits);
            numberOfUnits--;
        }
    };
    Wave.prototype.calculate = function () {
        this.calculateNumberOfUnits();
        this.calculateHealthOfUnits();
        this.calculateSpeedOfUnits();
    };
    Wave.prototype.calculateNumberOfUnits = function () {
        this.totalNumberOfUnits = this.numberOfWave + 10;
        //Raises or decreases the number of units from 0 to 10% randomly
        this.totalNumberOfUnits += ((Math.random() / 2) - 0.25) * this.totalNumberOfUnits;
        this.averageNumberOfUnits = this.totalNumberOfUnits / this.waveDuration;
    };
    Wave.prototype.calculateSpeedOfUnits = function () {
        this.speedOfUnits = Math.sqrt(this.numberOfWave + 2);
    };
    //the health calculation is split into two different segements with different math functions to calculate it
    Wave.prototype.calculateHealthOfUnits = function () {
        if (this.numberOfWave <= this.healthFunctionSwitch) {
            this.healthOfUnits = this.healthFunctionEarly();
        }
        else {
            this.healthOfUnits = this.healthFunctionLate();
        }
    };
    Wave.prototype.healthFunctionEarly = function () {
        return 0.1 * this.numberOfWave * this.numberOfWave + 15;
    };
    Wave.prototype.healthFunctionLate = function () {
        return 15 * Math.sqrt(this.numberOfWave - 20) + 50;
    };
    Wave.prototype.getNumberOfWave = function () {
        return this.numberOfWave;
    };
    Wave.prototype.getProgressOfCooldown = function () {
        return Math.floor((this.coolDown / 600) * 6);
    };
    return Wave;
}());
/// <reference path="games/Game.ts" />
/// <reference path="games/Processor.ts" />
/// <reference path="games/Stats.ts" />
/// <reference path="games/Balance.ts" />
/// <reference path="start.ts" />
/// <reference path="towers/Tower.ts" />
/// <reference path="towers/BombTower.ts" />
/// <reference path="towers/FrozenTower.ts" />
/// <reference path="towers/HomingTower.ts" />
/// <reference path="towers/LightningTower.ts" />
/// <reference path="towers/LinkTower.ts" />
/// <reference path="towers/MGTower.ts" />
/// <reference path="towers/SniperTower.ts" />
/// <reference path="towers/TowerCosts.ts" />
/// <reference path="towers/TowerType.ts" />
/// <reference path="shots/Shot.ts" />
/// <reference path="shots/SingleTargetShot.ts" />
/// <reference path="shots/Connection.ts" />
/// <reference path="shots/DirectedBall.ts" />
/// <reference path="shots/ExplosiveShot.ts" />
/// <reference path="shots/FrozenSurface.ts" />
/// <reference path="shots/Lightning.ts" />
/// <reference path="shots/StraightBall.ts" />
/// <reference path="shots/SniperShot.ts" />
/// <reference path="drawing/Drawer.ts" />
/// <reference path="drawing/Pictures.ts" />
/// <reference path="tools/Constants.ts" />
/// <reference path="tools/MouseControl.ts" />
/// <reference path="units/SlowEffect.ts" />
/// <reference path="units/Unit.ts" />
/// <reference path="units/Wave.ts" />
//# sourceMappingURL=viren.js.map